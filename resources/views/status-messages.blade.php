<div class="container">
@if( Session('status') ) 
<div class="alert alert-success">
  {{ Session('status') }}
</div>
@endif
@if( Session('error') ) 
<div class="alert alert-danger">
  {{ Session('error') }}
</div>
@endif
</div>