@extends('layouts.admin')
@section('title', 'UNHRD Dashboard - RSS Items')
@section('header')
    <div class="clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> RSSItems
            <a class="btn btn-success pull-right" href="{{ route('admin.rss_items.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
            <a class="btn btn-success pull-right" href="{{ route('admin.rss_items.import') }}"><i class="glyphicon glyphicon-plus"></i> Import</a>
        </h1>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($rss_items->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>Text</th>
                        <th>Visible</th>
                        <th>Posted Date</th>
                        <th>Expiration Date</th>

                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($rss_items as $rss_item)
                            <tr>
                                <td>{{$rss_item->text}}</td>
                                <td>{{ $rss_item->visible ? 'Yes' : 'No' }}</td>
                                <td>{{$rss_item->posted_date}}</td>
                                <td>{{$rss_item->expiration_date}}</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.rss_items.show', $rss_item->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                    <a class="btn btn-xs btn-warning" href="{{ route('admin.rss_items.edit', $rss_item->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <form action="{{ route('admin.rss_items.destroy', $rss_item->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $rss_items->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection