@extends('layouts.admin')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> RSSItems / Edit #{{$rss_item->id}}</h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('admin.rss_items.update', $rss_item->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('text')) has-error @endif">
                       <label for="text-field">Text</label>
                    <input type="text" id="text-field" name="text" class="form-control" value="{{ is_null(old("text")) ? $rss_item->text : old("text") }}"/>
                       @if($errors->has("text"))
                        <span class="help-block">{{ $errors->first("text") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('visible')) has-error @endif">
                       <label for="visible-field">Visible</label>
                    <div class="btn-group" data-toggle="buttons"><label class="btn btn-primary"><input type="radio" value="true" name="visible" id="visible-field" autocomplete="off"> True</label><label class="btn btn-primary active"><input type="radio" name="visible" value="false" id="visible-field" autocomplete="off"> False</label></div>
                       @if($errors->has("visible"))
                        <span class="help-block">{{ $errors->first("visible") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('expiration_date')) has-error @endif">
                       <label for="expiration_date-field">Expiration_date</label>
                    <input type="text" id="expiration_date-field" name="expiration_date" class="form-control date-picker" value="{{ is_null(old("expiration_date")) ? $rss_item->expiration_date : old("expiration_date") }}"/>
                       @if($errors->has("expiration_date"))
                        <span class="help-block">{{ $errors->first("expiration_date") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('admin.rss_items.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
