@extends('layouts.admin')
@section('header')
<div class="clearfix">
        <h1>RSSItems / Show #{{$rss_item->id}}</h1>
        <form action="{{ route('admin.rss_items.destroy', $rss_item->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('admin.rss_items.edit', $rss_item->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                     <label for="text">Text</label>
                     <p class="form-control-static">{{$rss_item->text}}</p>
                </div>
                    <div class="form-group">
                     <label for="visible">Visible</label>
                     <p class="form-control-static">{{$rss_item->visible}}</p>
                </div>
                <div class="form-group">
                     <label for="expiration_date">Posted Date</label>
                     <p class="form-control-static">{{$rss_item->posted_date}}</p>
                </div>
                <div class="form-group">
                     <label for="expiration_date">Expiration Date</label>
                     <p class="form-control-static">{{$rss_item->expiration_date}}</p>
                </div>
                <div class="well well-sm clearfix">
                    <a class="btn btn-link pull-right" href="{{ route('admin.rss_items.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>
        </div>
    </div>

@endsection