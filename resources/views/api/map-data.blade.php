{
  "map": "worldLow",
  "areas": [
    @for ($i = 0; $i < count($mapData["pairs"]); $i++)
    <?php $pair = $mapData["pairs"][$i]; ?>
    {
      "id": "{{ $pair[1] }}",
      "showAsSelected": true,
      "selectedColor": "{{ config("unhrd.api.destination_country") }}"
    }
    @if ($i != count($mapData["pairs"])-1 )
    ,
    @endif
    @endfor
  ],
  "lines": [
    @for ($i = 0; $i < count($mapData["coordinates"]); $i++)
    <?php $coordinates = $mapData["coordinates"][$i]; ?>
    {
      "id": "{{ $coordinates[2] }}",
      "arc": -0.85,
      "alpha": 0.8,
      "latitudes": [{{ $coordinates[0][0] }}, {{ $coordinates[1][0] }}],
      "longitudes": [{{ $coordinates[0][1] }}, {{ $coordinates[1][1] }}]
    }
    @if ($i != count($mapData["coordinates"])-1 )
    ,
    @endif
    @endfor
  ],
  "images": [
    @for ($i = 0; $i < count($mapData["depots"]); $i++)
    <?php $depot = array_values($mapData["depots"])[$i]; ?>
    {
      "svgPath": "M8.75,0,0,3.58V11.5H7.42V4.42H15.5V11.5h2V3.58ZM5.44,8H2V5.75H5.44Z",
      "latitude":  {{ $depot["coordinates"][0] }},
      "longitude": {{ $depot["coordinates"][1] }},
      "label": "{{ $depot["name"]}}",
      "labelPosition": "bottom",
      "labelColor": "{{ config("unhrd.api.depot_country") }}",
      "labelFontSize": 14,
      "scale": 2
    }
    @if ($i != count($mapData["depots"])-1 )
    ,
    @endif
    @endfor
  ]
}