@extends('layouts.tv')

@section('title', 'UNHRD TV Dashboard - Page 2')
<meta http-equiv="refresh" content="{{ config('unhrd.refresh_interval') }};url={{ url('tv/page3') }}" >
@section('content')
<div class="grid grid--from-a1 grid--to-d2">
  <header>
    <h1 class="pull-left">
      {{ config('unhrd.page2.title') }}
      <!--<span class="primary" style="font-size: 16px;">{{ $timeFrame }}</span>-->

      @if(date('j') == config('unhrd.orange_day') || (isset($_GET['day']) && date('j') == $_GET['day'] ) )
      <p class="orange-day-title">{{ config('unhrd.orange_day_sentence') }}</p>
      @endif
    </h1>
      
  </header>
      
</div>

<div class="grid grid--from-a2 grid--to-d9 slideshow" style="margin-left: 10px">
  <div class="panel panel-transparent marquee_up">
      <h3>{{ config('unhrd.page2.nfi_shipped_per_partner') }}</h3>
    <div class="panel-body ">
          @foreach($nfiShippedPerPartner as $item)
              @include('tv.parts.progress-bar', ['progressBar' => $item])
          @endforeach
    </div>
  </div>
</div>
<div class="grid grid--from-g1 grid--to-h1 columns">
    <br>
 {{ Html::Image('/images/logo.svg', "UNHRD", array('class' => 'pull-right logo', 'style' => '')) }}
</div>
<div class="grid grid--from-e1 grid--to-h3 columns">
    <br><br><br><br>
  <h3>{{ config('unhrd.page1.goods_per_sales_office') }}</h3>
  <div class="row charts">
    <div class="charts-container">
      @foreach($nfiShippedPerHub as $item)
      <div class="col-xs-2">
        @include('tv.parts.pie-chart', ['chart' => $item])
      </div>
      @endforeach
    </div>
  </div>
</div>
<div class="grid grid--from-e3 grid--to-h6">
    <br><br><br><br>
  <h3>{{ config('unhrd.page2.top_nfi_shipped') }}</h3>
  @foreach(array_chunk($nfiTopFiveShipped, 2) as $row)
  <div class="row charts">
    @foreach($row as $item)
    <div class="col-md-1"></div>
    <div class="col-md-4">
      @include('tv.parts.icon-panel', ['panel' => $item])
    </div>
    @endforeach
  </div>
  @endforeach
</div>

<div class="grid grid--from-e6 grid--to-h9">
    <br>
    <div class="panel panel-transparent">
      <h3>{{ config('unhrd.page2.top_suppliers') }}</h3>
      <div class="panel-body">
        @foreach($nfiTopFiveSuppliers as $item)
            @include('tv.parts.progress-bar', ['progressBar' => $item])
        @endforeach
      </div>
    </div>
</div>

<div class="grid grid--from-i1 grid--to-k10 figures figures-page-2" style="margin-left: 60px">
    <h2>{{ config('unhrd.page2.nfi_dispatched_total') }}</h2>
  <ul>
      <br>
    <li>{{ config('unhrd.page2.total_weight') }} <span>Total Weight (MT)</span></li>
    <li>{{ config('unhrd.page2.total_volume') }}<span>Total Volume (m<sup>3</sup>)</span></li>
    <li>{{ config('unhrd.page2.total_value') }} <span>Total Value (USD)</span></li>
    <li>{{ config('unhrd.page2.countries') }} <span>Countries Reached</span></li>
    <li>{{ config('unhrd.page2.shipments') }} <span>Shipments</span></li>
    <li>{{ config('unhrd.page2.partners') }} <span>Number of partners served</span></li>
  </ul>
    <br>

  <h2>{{ config('unhrd.page2.loan_and_borrows') }}</h2>
  <ul> 
    <li>{{ $annualFigures['totalLoans'] }} <span>{{ config('unhrd.page2.loan_and_borrows_total') }}</span></li>
    <li>{{ $annualFigures['totalLoanedPartners'] }} <span>{{ config('unhrd.page2.loan_and_borrows_partners') }}</span></li>
  </ul>
  <br>
  <h2>{{ config('unhrd.page2.rrt') }}</h2>
  <ul> 
    <li>{{ config('unhrd.page2.rrt_deployment_days_value') }} <span>{{ config('unhrd.page2.rrt_deployment_days') }}</span></li>
    <li>{{ config('unhrd.page2.rrt_countries_value') }}<span>{{ config('unhrd.page2.rrt_countries') }}</span></li>
    <li>{{ config('unhrd.page2.rrt_trainings_held_value') }}<span>{{ config('unhrd.page2.rrt_trainings_held') }}</span></li>
  </ul>
</div>
{{ Html::script('js/jquery.marquee.min.js') }}
<script type="text/javascript">
          $(document).ready(function() {
            $('.marquee_up').marquee({
              //speed in milliseconds of the marquee
                duration: 30000,
                //gap in pixels between the tickers
                gap: 5,
                //time in milliseconds before the marquee will start animating
                delayBeforeStart: 0,
                //'left' or 'right'
                direction: 'up'
                //true or false - should the marquee be duplicated to show an effect of continues flow
//                duplicated: true
            });
          });

        </script>
@endsection