@extends('layouts.tv')

@section('title', 'UNHRD TV Dashboard - Page 3')
<meta http-equiv="refresh" content="{{ config('unhrd.refresh_interval') }};url={{ url('tv/page4') }}" >
@section('content')
<div class="grid grid--from-a1 grid--to-j2">
  <header>
    <h1 class="pull-left">
      {{ config('unhrd.page3.title') }}
      @if(date('j') == config('unhrd.orange_day') || (isset($_GET['day']) && date('j') == $_GET['day'] ) )
      <p class="orange-day-title">{{ config('unhrd.orange_day_sentence') }}</p>
      @endif
    </h1>
    {{ Html::Image('/images/logo.svg', "UNHRD", array('class' => 'pull-right logo', 'style' => '')) }}
  </header>
</div>

<div class="grid grid--from-d2 grid--to-f7 marquee_up">
  @include('tv.parts.overview-table', ['item' => $overviewTable])
    </div>
</div>

<div class="grid grid--from-g4 grid--to-j7">
  <div class="panel panel-transparent">
      <br>
    <h3><strong>{{config('unhrd.page3.high_value_operations')}}</strong></h3>
    <div class="panel-body">
      @foreach($highValueOperations as $item)
      @include('tv.parts.progress-bar', ['progressBar' => $item])
      @endforeach
    </div>
  </div>
</div>

<div class="grid grid--from-g2 grid--to-j3">
  <div class="row">
    <div class="col-md-6">
        <a class="twitter-timeline" href="https://twitter.com/unhrd" 
           data-theme="dark" data-link-color="#fff" data-chrome="noscrollbar"></a>
    </div>
    <div class="col-md-6">
        <a class="twitter-timeline" href="https://twitter.com/WFPLogistics" 
           data-theme="dark" data-link-color="#fff" data-chrome="noscrollbar"></a>
    </div>
  </div>
      <script>
            ! function (d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0]
                , p = /^http:/.test(d.location) ? 'http' : 'https';
              if (!d.getElementById(id)) {
                js = d.createElement(s);
                js.id = id;
                js.src = p + "://platform.twitter.com/widgets.js";
                fjs.parentNode.insertBefore(js, fjs);
              }
            }(document, "script", "twitter-wjs");
      </script>
</div>

<div class="grid grid--from-a2 grid--to-c4">
  <h3><strong>{{config('unhrd.page3.high_value_po_goods')}}</strong></h3>
  @include('tv.parts.po-table', ['item' => $highValueOpenPOsGoods])
    </div>
<div class="grid grid--from-a5 grid--to-c7">
    <h3><strong>{{config('unhrd.page3.high_value_po_trans')}}</strong></h3>
    <br>
  @foreach(array_chunk($highValueOpenPOsTransport, 2) as $row)
  <div class="row charts">
    @foreach($row as $item)
    <div class="col-md-1"></div>
    <div class="col-md-4">
      @include('tv.parts.icon-panel', ['panel' => $item])
    </div>
    @endforeach
  </div>
  @endforeach
</div>

<div class="grid grid--from-a8 grid--to-k9">
    <br><br>
    <div class="row">
        @foreach($imagePath as $item)
        <div class="col-md-2" id="piecemaker-container">
          {{ Html::Image('/uploads/'.$item),array('max-width' => 70 , 'max-height' => 30)}}
        </div>
        @endforeach
  </div>
</div>

{{ Html::script('js/jquery.marquee.min.js') }}
<script type="text/javascript">
          $(document).ready(function() {
            $('.marquee_up').marquee({
              //speed in milliseconds of the marquee
                duration: 30000,
                //gap in pixels between the tickers
                gap: 1,
                //time in milliseconds before the marquee will start animating
                delayBeforeStart: 0,
                //'left' or 'right'
                direction: 'up'
                //true or false - should the marquee be duplicated to show an effect of continues flow
//                duplicated: true
            });
          });

        </script>
@endsection