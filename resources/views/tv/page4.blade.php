@extends('layouts.tv')

@section('title', 'UNHRD TV Dashboard - Page 4')
<meta http-equiv="refresh" content="{{ config('unhrd.refresh_interval') }};url={{ url('tv/page1') }}" >
@section('content')
<video class="col-md-12" id="unhrd_video" width="auto" height="auto" 
       autoplay="autoplay" poster="/images/wfp_unhrd.jpeg">
  <source src='{{ url('images/unhrd_network.mp4') }}' type="video/mp4">
</video>
<script type="text/javascript">
          $(document).ready(function() {
            var myVid = document.getElementById("unhrd_video");
            myVid.load();
            myVid.play();
          });

        </script>
@endsection


