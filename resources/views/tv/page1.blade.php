@extends('layouts.tv')

@section('title', 'UNHRD TV Dashboard - Page 1')
<meta http-equiv="refresh" content="{{ config('unhrd.refresh_interval') }};url={{ url('tv/page2') }}" >

@section('content')

<div class="grid grid--from-a1 grid--to-f8">
  @include('tv.parts.map', ['mapData' => $mapData])
</div>

<div class="grid grid--from-a1 grid--to-f2">
  <header>
    <h1 class="pull-left">
      {{ config('unhrd.page1.title') }}
      <span class="primary"> {{ $date }}</span>
      @if(date('j') == config('unhrd.orange_day') || (isset($_GET['day']) && date('j') == $_GET['day'] ) )
      <p class="orange-day-title">{{ config('unhrd.orange_day_sentence') }}</p>
      @endif
    </h1>
    <!--{{ Html::Image('/images/stm.svg', "UNHRD", array('class' => 'pull-right logo', 'style' => '')) }}-->
    {{ Html::Image('/images/logo.svg', "UNHRD", array('class' => 'pull-right logo', 'style' => '')) }}
  </header>
</div>
<div class="grid grid--from-g1 grid--to-h6 figures">
  <h2>{{ config('unhrd.page1.figures_box') }}</h2>
  <ul>
    <li>{{ $currentFigures['totalWeight'] }}  <span>Total Weight (MT)</span></li>
    <li>{{ $currentFigures['totalVolume'] }} <span>Total Volume (m<sup>3</sup>)</span></li>
    <li>{{ $currentFigures['totalValue'] }} <span>Total Value (USD)</span></li>
    <li>{{ $currentFigures['countriesReached'] }} <span>Countries Reached</span></li>
    <li>{{ $currentFigures['shipments'] }} <span>Shipments</span></li>
    <li>{{ $currentFigures['totalPartners'] }} <span>Number of partners served</span></li>
  </ul>
</div>
<div class="grid grid--from-a7 grid--to-c9">
  @include('tv.parts.column-chart', ['chart' => $goodsPerDestinationChart])
</div>
<div class="grid grid--from-d7 grid--to-f9">
  @include('tv.parts.column-chart', ['chart' => $goodsPerSalesOfficeChart])
</div>
<div class="grid grid--from-g7 grid--to-j9 columns">
    <h3>{{ config('unhrd.page1.top_cluster_items') }}</h3><br><br>
  <div class="row charts">
    <div class="charts-container">
      @foreach($topItemsPerSector as $item)
      <div class="col-xs-3">
        @include('tv.parts.pie-chart', ['chart' => $item])
      </div>
      @endforeach
    </div>
  </div>
</div>
<div class="grid grid--from-i1 grid--to-j7 partners">
  <h2>{{ config('unhrd.page1.partners_section') }}</h2>
  <ul>
      <div class="marquee_up">
    @foreach($goodsPerPartner as $partner)
    <li>
      @include('tv.parts.partner-bubble', ['partner' => $partner])
    </li>
    @endforeach
      </div>
  </ul>
</div>
{{ Html::script('js/jquery.marquee.min.js') }}
<script type="text/javascript">
          $(document).ready(function() {
            $('.marquee_up').marquee({
              //speed in milliseconds of the marquee
                duration: 30000,
                //gap in pixels between the tickers
                gap: 5,
                //time in milliseconds before the marquee will start animating
                delayBeforeStart: 0,
                //'left' or 'right'
                direction: 'up'
                //true or false - should the marquee be duplicated to show an effect of continues flow
//                duplicated: true
            });
          });

        </script>
@endsection