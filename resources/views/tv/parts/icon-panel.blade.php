<div class="media">
  <div class="media-left">
  {{ Html::Image($panel->icon, $panel->title, array('class' => 'media-object')) }}
  </div>
  <div class="media-body">
    <h3 class="media-heading">{{ $panel->title }}</h3>
    <h3 class="value">{{ $panel->value }}</h3>
  </div>
</div>
<hr>