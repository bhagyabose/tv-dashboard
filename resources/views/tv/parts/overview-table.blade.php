<div class="panel panel-transparent">
  <h3>{{ $item->title }}</h3>
  <div class="panel-body">
    @foreach($item->rows as $row)
    <div class="media">
        <?php
        $date = strtotime($row['start_date']);
        $start_date = ' - From '.date('d-m-Y',$date);
        if($start_date === ' - From 01-01-1970'){
            $start_date = '';
        }
        ?>
        <h3>{{ $row['name'] }}{{$start_date}}</h3>
      <ul class="nav nav-justified">
        <li><h4 class="level">Type:{{ $row['level'] }}</h4></li> 
        <li><h4 class="tons">{{ $row['weight'] }} MT</h4></li>
        <li><h4 class="value">{{ $row['value'] }}M USD</h4></li>
        <li><h4 class="volume">{{ $row['volume'] }} Shipments</h4></li>
        <li><h4 class="partners">{{ $row['partners'] }} Partners</h4></li>
      </ul>
    </div>
    <hr>
    @endforeach
  </div>
</div>