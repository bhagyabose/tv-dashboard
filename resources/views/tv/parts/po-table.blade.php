 <div class="panel panel-transparent">
   <!--<h3>{{ $item->title }}</h3>-->
   <table class="table">
    <thead>
        <tr>
        @foreach($item->header as $header)
        <th>{{ $header }}</th>
        @endforeach
      </tr>
    </thead>
    <tbody style="color:#FFF">
      @foreach($item->values as $line)
      <tr  style="border-bottom: 1px;border-bottom-color: whitesmoke">
        @foreach($line as $value)
          <td>{{ $value }}</td>
        @endforeach
      </tr>
      @endforeach
    </tbody>
  </table>
</div>