<h3>{{ $chart->title }}</h3>
<div id="{{ $chart->divId }}" class="column-chart" ></div>
<script type="text/javascript">
  $(document).ready(function() {
    AmCharts.makeChart('{{ $chart->divId }}',
    {
      'type': 'serial',
      'balloonDateFormat': '',
      'categoryField': 'category',
      'plotAreaBorderColor': 'transparent',
      'startDuration': 1,
      'backgroundColor': 'rgba(35, 40, 54, 0.7)',
      'borderColor': 'transparent',
      'creditsPosition': 'bottom-right',
      'fontFamily': 'Lato',
      'theme': 'dark',
      'color': 'white',
      'categoryAxis': {
        'autoRotateCount': 10,
        'gridPosition': 'start',
        'minHorizontalGap': 83,
        'minVerticalGap': 34,
        'title': ' ',
        'titleBold': false
      },
      'trendLines': [],
      'graphs': [
      {
        'balloonText': '',
        'fillAlphas': 1,
        'id': 'AmGraph-1',
        'title': 'graph 1',
        'type': 'column',
        'valueField': 'column-1',
        'fillColors': '{{ $chart->fillColors }}',
          'showAllValueLabels': true,
        'lineColor': 'transparent',
        "labelText": "[[value]] {{ $chart->unit }}"
      }
      ],
      'guides': [],
      'valueAxes': [{
        "labelsEnabled": false,
        "logarithmic": true,
      }],
      'allLabels': [],
      'balloon': {},
      'dataProvider': [
      @foreach ($chart->items as $item)
      {
        'category': "{{ $item['label'] }}",
        'column-1': {{ $item['value'] }}
      },
      @endforeach
      ]
    }
    );
  })
</script>
