<div class="pie-chart">
	<h2 class="value">{{ $chart->value }} {{ $chart->unit }}</h2>
	<div id="{{ $chart->divId }}" class="chart"></div>
</div>
<h4 class="description">{{ $chart->label }}</h4>

<script type="text/javascript">
$(document).ready(function() {
AmCharts.makeChart('{{ $chart->divId }}',
	{
		'type': 'pie',
		'balloonText': '',
		'innerRadius': '80%',
		'labelText': '',
		'pullOutRadius': 0,
		'startRadius': 0,
		'colors': [
			'{{ $chart->fillColor }}',
			'#232836'
		],
		'titleField': 'category',
		'valueField': 'column-1',
		'backgroundColor': '#232836',
		'borderColor': '',
		'autoDisplay': true,
		'autoResize': true,
		'dataProvider': [
		{
			'column-1': '{{ $chart->value }}'
		},
		{
			'column-1': '{{ 100-$chart->value }}'
		}
		]
	}
	);
});
</script>