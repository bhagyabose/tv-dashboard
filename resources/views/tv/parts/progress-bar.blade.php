<div class="row">
  <div class="col-md-10">
    <div class="progress">
      <div class="progress-bar" role="progressbar" 
           aria-valuenow="{{ $progressBar->percentage }}" aria-valuemin="0" 
           aria-valuemax="500" style="width: {{ $progressBar->percentage }}%; 
           background-color: {{ $progressBar->color }}">
        <span class="sr-only">{{ $progressBar->percentage }}% Complete</span>
      </div>
    </div>

    <span class="value">{{ $progressBar->green }} {!! $progressBar->units['green'] or "" !!}</span>
    <span class="volume">{{ $progressBar->yellow }} {!! $progressBar->units['yellow'] or "" !!}</span>
    <span class="partner pull-right">{{ $progressBar->white }} {!! $progressBar->units['white'] or "" !!}</span>
  </div>

  <div class="col-md-2 tons">
    {{ $progressBar->blue }} {!! $progressBar->units['blue'] or "" !!}
  </div>
</div>
