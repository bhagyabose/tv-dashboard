<h3>{{ $item->title }}</h3>
@foreach($item->values as $line)
<div class="media">
  <div class="media-body">
    <h3 class="media-heading">{{ $line['title'] }}</h3>
    <h3 class="value">{{ $line['value'] }}</h3>
  </div>
</div>
<hr>
@endforeach