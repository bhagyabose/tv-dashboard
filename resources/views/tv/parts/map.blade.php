  <div class="map" id="map"></div>
  <style type="text/css">
    #map {
      height: 100%;
      width: 100%;
    }
  </style>
  <script type="text/javascript">
    $(document).ready(function() {
      var targetSVG = 'M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z';
      var planeSVG = 'm2,106h28l24,30h72l-44,-133h35l80,132h98c21,0 21,34 0,34l-98,0 -80,134h-35l43,-133h-71l-24,30h-28l15,-47';
      var depotSVG = 'M8.75,0,0,3.58V11.5H7.42V4.42H15.5V11.5h2V3.58ZM5.44,8H2V5.75H5.44Z';

      window.map = AmCharts.makeChart('map', {
        type: 'map',
        dragMap: true,

        dataLoader: {
          "url": "{{ route('map_data') }}" 
        },
        areasSettings: {
          unlistedAreasColor: '#506580',
          unlistedAreasOutlineColor: '#434447',
          outlineColor: '#04111A',
          outlineThickness: 1
        },

        imagesSettings: {
          color: '{{ config("unhrd.page1.depot_country") }}'
        },

        linesSettings: {
          color: '{{ config("unhrd.page1.line_color") }}',
          alpha: 1
        },

        export: {
          enabled:true
        }
      });
    });
  </script>