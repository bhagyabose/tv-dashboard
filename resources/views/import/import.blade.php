@extends('layouts.admin')

@section('title', 'UNHRD Dashboard - Importer')

@section('header')
    <div class="clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> Dashboard Importer
        </h1>
    </div>
@endsection

@section('content')

{{ Form::open(['action' => "ImportController@importPost", 'method' => 'post', 'files' => true]) }}
	<div class="form-group">
		<label for="report">File to import <span class="text-danger">(must be an Excel file in XLSX format)</span></label>
		<input type="file" id="report" name="report">
		<p class="help-block">This file should be either a POD Report, PO Report or a Logistics Consolidation Report from Wings.</p>
	</div>
	<div class="form-group">
		<label for="fileType">Type of file</label>
		<div class="radio">
			<label for="fileTypePOD">
				<input type="radio" id="fileTypePOD" name="fileType" value="pod" checked>
				POD Report
			</label>
		</div>
                <div class="radio">
			<label for="fileTypePODAnnual">
				<input type="radio" id="fileTypePOD" name="fileType" value="pod_annual">
				POD Annual Report
			</label>
		</div>
		<div class="radio">
			<label for="fileTypeLCR">
				<input type="radio" id="fileTypeLCR" name="fileType" value="lcr">
				Logistics Consolidation Report
			</label>
		</div>
		<div class="radio">
			<label for="fileTypePOR">
				<input type="radio" id="fileTypePOR" name="fileType" value="por">
				PO Report (ZSCR007)
			</label>
		</div>
                
		<p class="help-block"></p>
	</div>
	<button type="submit" class="btn btn-primary">Import</button>
{{ Form::close() }}



@endsection