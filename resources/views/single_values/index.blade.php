@extends('layouts.admin')
@section('title', 'UNHRD Dashboard - Single Values')
@section('header')
    <div class="clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> Single Values
            <a class="btn btn-success pull-right" href="{{ route('admin.single_values.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
        </h1>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($single_values->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>KEY</th>
                            <th>VALUE</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($single_values as $single_value)
                            <tr>
                                <td>{{$single_value->key}}</td>
                                <td>{{$single_value->value}}</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.single_values.show', $single_value->key) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                    <a class="btn btn-xs btn-warning" href="{{ route('admin.single_values.edit', $single_value->key) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <form action="{{ route('admin.single_values.destroy', $single_value->key) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $single_values->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection