<!doctype html>
<html class="no-js" lang="">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=0.68">
    <title>@yield('title')</title>

    {{ Html::style('css/app.css') }}
  </head>
  <body>
    <!--[if lt IE 10]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <main class="container-fluid">
      @yield('content')
    </main>

    <div class="news-ticker">
      <p><i class="fa fa-rss"></i> Innovation at DIHAD | 5 Things You May Not Know About UNHRD | Moving Forward in Latin America | Irish Aid Steps Up its Stockpiling Operations at UNHRD | Optimizing UNHRD’s</p>
    </div>

    <footer>
      <span>The UNHRD Network is generously supported by: </span>
      <img src="images/flags/ireland.png" alt="Ireland" />
      <img src="images/flags/italy.png" alt="Italy" />
      <img src="images/flags/malaysia.png" alt="Malaysia" />
      <img src="images/flags/norway.png" alt="Norway" />
      <img src="images/flags/spain.png" alt="Spain" />
      <img src="images/flags/switzerland.png" alt="Switzerland" />
      <img src="images/flags/uae.png" alt="United Arab Emirates" />
      <img src="images/flags/panama.png" alt="Panama" />
      <img src="images/flags/ghana.png" alt="Ghana" />
    </footer>


    <!-- build:js scripts/vendor.js -->
    <!-- bower:js -->
    <!-- endbower -->
    <!-- endbuild -->

    <!-- build:js scripts/plugins.js -->
    <script type="text/javascript" src="http://www.amcharts.com/lib/3/amcharts.js"></script>
	  <script type="text/javascript" src="http://www.amcharts.com/lib/3/pie.js"></script>
    <script type="text/javascript" src="http://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/ammap.js"></script>
    <script src="https://www.amcharts.com/lib/3/maps/js/worldLow.js"></script>
    <!-- endbuild -->

    {{ Html::script('js/app.js') }}
  </body>
</html>
