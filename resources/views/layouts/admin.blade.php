<!doctype html>
<html class="no-js" lang="">
<head>
  <meta charset="utf-8">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@yield('title')</title>

  <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
  {{ Html::style('css/import.css') }}
</head>
<body>
    <!--[if lt IE 10]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
      <![endif]-->

      <nav class="navbar navbar-default">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">UNHRD Dashboard</a>
          </div>
          <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
              <li><a href="{{ route('admin.import.index') }}">Import</a></li>
              <li><a href="{{ route('admin.rss_items.index') }}">RSS Items</a></li>
              <li><a href="{{ route('admin.partners.index') }}">Partners</a></li>
              <li><a href="{{ route('admin.single_values.index') }}">Single Values</a></li>
              <li><a href="{{ route('admin.operations.index') }}">Operations</a></li>
              <li><a href="{{ route('admin.snap_shots.index') }}">Image Importer</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </nav>

      @include('status-messages')

      <main class="container">
        @yield('header')
        @yield('content')
      </main>


      {{ Html::script('js/admin.js') }}
      <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
      <!-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> -->
    </body>
    </html>
