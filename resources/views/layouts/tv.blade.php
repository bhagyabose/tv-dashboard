<!doctype html>
<html class="no-js" lang="">
<head>
  <meta charset="utf-8">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=0.7">
  <!-- <meta http-equiv="refresh" content="{{ config('unhrd.refresh_interval') }}"> -->
  <title>@yield('title')</title>

  {{ Html::style('css/tv.css') }}
  <link rel="stylesheet" href="https://opensource.keycdn.com/fontawesome/4.7.0/font-awesome.min.css" 
        integrity="sha384-dNpIIXE8U05kAbPhy3G1cz+yZmTzA6CY8Vg/u2L9xRnHjJiAK76m2BIEaSEV+/aU" crossorigin="anonymous">

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<body>
    <!--[if lt IE 10]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
      <![endif]-->
      <div id="wrapper">
        <main class="container-fluid">
          @yield('content')
        </main>

        <div class="grid grid--from-a10 grid--to-j10">
          @if(date('j') == config('unhrd.orange_day') || (isset($_GET['day']) && date('j') == $_GET['day'] ) )
          <div class="marquee orange-day">
            @else
            <div class="marquee">
              @endif
              {{ $rssItemsString }}
            </div>

            <footer>
              <span>The UNHRD Network is generously supported by: </span>
              {{ Html::Image('/images/flags/ireland.png', "Ireland") }}
              {{ Html::Image('/images/flags/italy.png', "Italy") }}
              {{ Html::Image('/images/flags/malaysia.png', "Malaysia") }}
              {{ Html::Image('/images/flags/norway.png', "Norway") }}
              {{ Html::Image('/images/flags/spain.png', "Spain") }}
              {{ Html::Image('/images/flags/switzerland.png', "Switzerland") }}
              {{ Html::Image('/images/flags/uae.png', "United Arab Emirates") }}
              {{ Html::Image('/images/flags/panama.png', "Panama") }}
              {{ Html::Image('/images/flags/ghana.png', "Ghana") }}
            </footer>
          </div>
        </div>

        <!-- build:js scripts/vendor.js -->
        <!-- bower:js -->
        <!-- endbower -->
        <!-- endbuild -->

        <!-- build:js scripts/plugins.js -->
        <script type="text/javascript" src="https://www.amcharts.com/lib/3/amcharts.js"></script>
        <script type="text/javascript" src="https://www.amcharts.com/lib/3/pie.js"></script>
        <script type="text/javascript" src="https://www.amcharts.com/lib/3/serial.js"></script>
        <script type="text/javascript" src="https://www.amcharts.com/lib/3/ammap.js"></script>
        <script type="text/javascript" src="https://www.amcharts.com/lib/3/maps/js/worldLow.js"></script>
        {{ Html::script('js/jquery.validate.js') }}
        {{ Html::script('js/jquery.marquee.min.js') }}
        {{ Html::script('js/bootstrap.min.js') }}
        {{ Html::script('js/dataloader.min.js') }}

        <script type="text/javascript">
          $(document).ready(function() {
            $('.marquee').marquee({
              duration: 15000
            });
          });

        </script>
        <!-- endbuild -->

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-65855222-2', 'auto');
          ga('send', 'pageview');
        </script>
      </body>
      </html>
