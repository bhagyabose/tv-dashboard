@extends('layouts.admin')

@section('title', 'UNHRD Dashboard - Image Importer')

@section('header')
    <div class="clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> Image Importer
        </h1>
    </div>
@endsection

@section('content')
{{ Form::open(['action' => "PhotoController@upload", 'method' => 'post', 'files' => true]) }}
        <div class="form-group">
            <label for="image">Upload Images - To Display on TV Dashboard</label>
        </div>
        <div class="form-group">
            <input type="file" id="image" name="image">
        </div>
        <button type="submit" class="btn btn-success">Upload</button>
{{ Form::close() }}

@endsection