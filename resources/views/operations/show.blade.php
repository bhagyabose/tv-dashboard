@extends('layouts.admin')
@section('header')
<div class="page-header">
        <h1>Operations / Show #{{$operation->id}}</h1>
        <form action="{{ route('admin.operations.destroy', $operation->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('admin.operations.edit', $operation->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static"></p>
                </div>
                <div class="form-group">
                     <label for="name">NAME</label>
                     <p class="form-control-static">{{$operation->name}}</p>
                </div>
                <div class="form-group">
                     <label for="start_date">Serving Since</label>
                     <p class="form-control-static">{{$operation->start_date}}</p>
                </div>
                <div class="form-group">
                     <label for="level">Emergency Level</label>
                     <p class="form-control-static">{{$operation->level}}</p>
                </div>
                <div class="form-group">
                     <label for="volume">No: of Shipments</label>
                     <p class="form-control-static">{{$operation->volume}}</p>
                </div>
                <div class="form-group">
                     <label for="weight">WEIGHT (MT)</label>
                     <p class="form-control-static">{{$operation->weight}}</p>
                </div>
                    <div class="form-group">
                     <label for="value">VALUE (M USD)</label>
                     <p class="form-control-static">{{$operation->value}}</p>
                </div>
                <div class="form-group">
                     <label for="partners">PARTNERS</label>
                     <p class="form-control-static">{{$operation->partners}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('admin.operations.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection