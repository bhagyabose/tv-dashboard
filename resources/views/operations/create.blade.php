@extends('layouts.admin')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-plus"></i> Operations / Create </h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('admin.operations.store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('name')) has-error @endif">
                       <label for="name-field">Name</label>
                    <input type="text" id="name-field" name="name" class="form-control" value="{{ old("name") }}"/>
                       @if($errors->has("name"))
                        <span class="help-block">{{ $errors->first("name") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('start_date')) has-error @endif">
                       <label for="start_date">Serving Since</label>
                    <input type="date" id="start_date" name="start_date" class="form-control" value="{{ old("start_date") }}"/>
                       @if($errors->has("start_date"))
                        <span class="help-block">{{ $errors->first("start_date") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('level')) has-error @endif">
                       <label for="level">Emergency Level</label>
                    <input type="text" id="level" name="level" class="form-control" value="{{ old("level") }}"/>
                       @if($errors->has("level"))
                        <span class="help-block">{{ $errors->first("level") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('volume')) has-error @endif">
                       <label for="volume-field">No: of Shipments</label>
                    <input type="text" id="volume-field" name="volume" class="form-control" value="{{ old("volume") }}"/>
                       @if($errors->has("volume"))
                        <span class="help-block">{{ $errors->first("volume") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('weight')) has-error @endif">
                       <label for="weight-field">Weight (MT)</label>
                    <input type="text" id="weight-field" name="weight" class="form-control" value="{{ old("weight") }}"/>
                       @if($errors->has("weight"))
                        <span class="help-block">{{ $errors->first("weight") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('value')) has-error @endif">
                       <label for="value-field">Value (M USD)</label>
                    <input type="text" id="value-field" name="value" class="form-control" value="{{ old("value") }}"/>
                       @if($errors->has("value"))
                        <span class="help-block">{{ $errors->first("value") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('partners')) has-error @endif">
                       <label for="partners-field">Partners</label>
                    <input type="text" id="partners-field" name="partners" class="form-control" value="{{ old("partners") }}"/>
                       @if($errors->has("partners"))
                        <span class="help-block">{{ $errors->first("partners") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('admin.operations.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
