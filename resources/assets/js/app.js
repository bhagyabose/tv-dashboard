// window.$ = window.jQuery = require('jquery');

// require('jquery-validation');

require('jquery');
require('bootstrap-sass');


// Instantiate the Bootstrap carousel
$('.multi-item-carousel').carousel({
  interval: false
});

// for every slide in carousel, copy the next slide's item in the slide.
// Do the same for the next, next item.
$('.multi-item-carousel .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));

  if (next.next().length>0) {
    next.next().children(':first-child').clone().appendTo($(this));
  } else {
  	$(this).siblings(':first').children(':first-child').clone().appendTo($(this));
  }
});

$( document ).ready(function() {
	function countryLocationLabel(value, formattedValue, valueAxis) {
		return value.replace(", ", "\n");
	}

	AmCharts.makeChart('chart1',
	{
		'type': 'pie',
		'balloonText': '',
		'innerRadius': '75%',
		'labelText': '',
		'pullOutRadius': 0,
		'startRadius': 0,
		"color": "#FFF",
		'creditsPosition': 'bottom-right',
		'colors': [
		'#D62331',
		'#232836'
		],
		'titleField': 'category',
		'valueField': 'column-1',
		'backgroundColor': '#232836',
		'borderColor': '',
		'dataProvider': [
		{
			'column-1': '74'
		},
		{
			'column-1': '26'
		}
		]
	}
	);

	AmCharts.makeChart('chart2',
	{
		'type': 'pie',
		'balloonText': '',
		'innerRadius': '75%',
		'labelText': '',
		'pullOutRadius': 0,
		'startRadius': 0,
		"color": "#FFF",
		'creditsPosition': 'bottom-right',
		'colors': [
		'#EEB236',
		'#232836'
		],
		'titleField': 'category',
		'valueField': 'column-1',
		'backgroundColor': '#232836',
		'borderColor': '',
		'dataProvider': [
		{
			'column-1': '62'
		},
		{
			'column-1': '38'
		}
		]
	}
	);

	AmCharts.makeChart('chart3',
	{
		'type': 'pie',
		'balloonText': '',
		'innerRadius': '75%',
		'labelText': '',
		'pullOutRadius': 0,
		'startRadius': 0,
		"color": "#FFF",
		'creditsPosition': 'bottom-right',
		'colors': [
		'#009C7F',
		'#232836'
		],
		'titleField': 'category',
		'valueField': 'column-1',
		'backgroundColor': '#232836',
		'borderColor': '',
		'dataProvider': [
		{
			'column-1': '46'
		},
		{
			'column-1': '54'
		}
		]
	}
	);


	AmCharts.makeChart('chart4',
	{
		'type': 'pie',
		'balloonText': '',
		'innerRadius': '75%',
		'labelText': '',
		'pullOutRadius': 0,
		'startRadius': 0,
		"color": "#FFF",
		'creditsPosition': 'bottom-right',
		'colors': [
		'#00A3DA',
		'#232836'
		],
		'titleField': 'category',
		'valueField': 'column-1',
		'backgroundColor': '#232836',
		'borderColor': '',
		'dataProvider': [
		{
			'column-1': '74'
		},
		{
			'column-1': '26'
		}
		]
	}
	);

	AmCharts.makeChart('chart5',
	{
		'type': 'pie',
		'balloonText': '',
		'innerRadius': '75%',
		'labelText': '',
		'pullOutRadius': 0,
		'startRadius': 0,
		"color": "#FFF",
		'creditsPosition': 'bottom-right',
		'colors': [
		'#F4888B',
		'#232836'
		],
		'titleField': 'category',
		'valueField': 'column-1',
		'backgroundColor': '#232836',
		'borderColor': '',
		'dataProvider': [
		{
			'column-1': '62'
		},
		{
			'column-1': '38'
		}
		]
	}
	);

	AmCharts.makeChart('chart6',
	{
		'type': 'pie',
		'balloonText': '',
		'innerRadius': '75%',
		'labelText': '',
		'pullOutRadius': 0,
		'startRadius': 0,
		"color": "#FFF",
		'creditsPosition': 'bottom-right',
		'colors': [
		'#3E8767',
		'#232836'
		],
		'titleField': 'category',
		'valueField': 'column-1',
		'backgroundColor': '#232836',
		'borderColor': '',
		'dataProvider': [
		{
			'column-1': '46'
		},
		{
			'column-1': '54'
		}
		]
	}
	);

	AmCharts.makeChart('chart7',
	{
		'type': 'pie',
		'balloonText': '',
		'innerRadius': '75%',
		'labelText': '',
		'pullOutRadius': 0,
		'startRadius': 0,
		"color": "#FFF",
		'creditsPosition': 'bottom-right',
		'colors': [
		'#2A93FC',
		'#232836'
		],
		'titleField': 'category',
		'valueField': 'column-1',
		'backgroundColor': '#232836',
		'borderColor': '',
		'dataProvider': [
		{
			'column-1': '74'
		},
		{
			'column-1': '26'
		}
		]
	}
	);

	AmCharts.makeChart('chart8',
	{
		'type': 'pie',
		'balloonText': '',
		'innerRadius': '75%',
		'labelText': '',
		'pullOutRadius': 0,
		'startRadius': 0,
		"color": "#FFF",
		'creditsPosition': 'bottom-right',
		'colors': [
		'#3780B6',
		'#232836'
		],
		'titleField': 'category',
		'valueField': 'column-1',
		'backgroundColor': '#232836',
		'borderColor': '',
		'dataProvider': [
		{
			'column-1': '62'
		},
		{
			'column-1': '38'
		}
		]
	}
	);

	AmCharts.makeChart('column1',
	{
		'addClassNames': true,
		'type': 'serial',
		'balloonDateFormat': '',
		'categoryField': 'category',
		'plotAreaBorderColor': '#EFEBE2',
		'startDuration': 1,
		'backgroundColor': '#232836',
		'borderColor': '#EFEBE2',
		'creditsPosition': 'bottom-right',
		'fontFamily': 'Lato',
		"color": "#FFF",
		'theme': 'dark',
		'categoryAxis': {
			'autoRotateCount': 10,
			'minHorizontalGap': 83,
			'minVerticalGap': 34,
			"dashLength": 0,
			"gridColor": "",
			"gridCount": 0,
			"gridThickness": 0,
			"tickLength": 0,
			'title': ' ',
			'titleBold': true,
			'categoryFunction': countryLocationLabel
		},
		'trendLines': [],
		'graphs': [
		{
			'balloonText': '[[title]] of [[category]]:[[value]]',
			'fillAlphas': 1,
			'fillColors': '#2A93FC',
			'lineColor': '#2A93FC',
			'id': 'AmGraph-1',
			'title': 'graph 1',
			'type': 'column',
			'valueField': 'column-1'
		}
		],
		'guides': [],
		"valueAxes": [{
		    "axisAlpha": 0,
		    "gridAlpha": 0,
				"gridThickness": 0,
		    "labelsEnabled": false
		}],
		'allLabels': [],
		'balloon': {},
		'titles': ' ',
		'dataProvider': [
		{
			'category': 'Accra',
			'column-1': 7
		},
		{
			'category': "Brindisi",
			'column-1': 2
		},
		{
			'category': 'Dubai',
			'column-1': '5'
		},
		{
			'category': 'Kuala Lumpur',
			'column-1': '2'
		},
		{
			'category': 'Las Palmas',
			'column-1': '3'
		}
		]
	}
	);

AmCharts.makeChart('column2',
{
	'type': 'serial',
	'balloonDateFormat': '',
	'categoryField': 'category',
	'plotAreaBorderColor': '#EFEBE2',
	'startDuration': 1,
	'backgroundColor': '#232836',
	'borderColor': '#EFEBE2',
	'creditsPosition': 'bottom-right',
	'fontFamily': 'Lato',
	"color": "#FFF",
	'theme': 'dark',
	'categoryAxis': {
		'autoRotateCount': 10,
		'gridPosition': 'start',
		'minHorizontalGap': 83,
		'minVerticalGap': 34,
		'title': ' ',
		'titleBold': false,
		"dashLength": 0,
		"gridColor": "",
		"gridCount": 0,
		"gridThickness": 0,
		"tickLength": 0
	},
	'trendLines': [],
	'graphs': [
	{
		'balloonText': '',
		'fillAlphas': 1,
		'id': 'AmGraph-1',
		'title': 'graph 1',
		'type': 'column',
		'valueField': 'column-1',
		'fillColors': '#00BFA5',
		'lineColor': '#00BFA5'
	}
	],
	'guides': [],
	"valueAxes": [{
		"axisAlpha": 0,
		"gridAlpha": 0,
		"gridThickness": 0,
		"labelsEnabled": false
	}],
	'allLabels': [],
	'balloon': {},
	'dataProvider': [
	{
		'category': 'Djibouti',
		'column-1': 7
	},
	{
		'category': "Sana'a",
		'column-1': 2
	},
	{
		'category': 'Hodeidah',
		'column-1': 3
	},
	{
		'category': 'Athens',
		'column-1': 4
	},
	{
		'category': 'Belgrade',
		'column-1': 6
	},
	{
		'category': 'Erbil',
		'column-1': 2
	},
	{
		'category': 'Duhok',
		'column-1': 3
	}
	]
}
);

// svg path for target icon
var targetSVG = 'M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z';
// svg path for plane icon
var planeSVG = 'm2,106h28l24,30h72l-44,-133h35l80,132h98c21,0 21,34 0,34l-98,0 -80,134h-35l43,-133h-71l-24,30h-28l15,-47';


window.map = AmCharts.makeChart('map', {
	type: 'map',

	dataProvider: {
		map: 'worldLow',
		'creditsPosition': 'bottom-right',
		lines: [{
			id: 'line1',
			arc: -0.85,
			alpha: 0.3,
			latitudes: [48.8567, 43.8163, 34.3, 23],
			longitudes: [2.3510, -79.4287, -118.15, -82]
		}, {
			id: 'line2',
			alpha: 0,
			color: '#000000',
			latitudes: [48.8567, 43.8163, 34.3, 23],
			longitudes: [2.3510, -79.4287, -118.15, -82]
		}],
		images: [{
			svgPath: targetSVG,
			title: 'Paris',
			latitude: 48.8567,
			longitude: 2.3510
		}, {
			svgPath: targetSVG,
			title: 'Toronto',
			latitude: 43.8163,
			longitude: -79.4287
		}, {
			svgPath: targetSVG,
			title: 'Los Angeles',
			latitude: 34.3,
			longitude: -118.15
		}, {
			svgPath: targetSVG,
			title: 'Havana',
			latitude: 23,
			longitude: -82
		}, {
			svgPath: planeSVG,
			positionOnLine: 0,
			color: '#000000',
			alpha: 0.1,
			animateAlongLine: true,
			lineId: 'line2',
			flipDirection: true,
			loop: true,
			scale: 0.03,
			positionScale: 1.3
		}, {
			svgPath: planeSVG,
			positionOnLine: 0,
			color: '#fff',
			animateAlongLine: true,
			lineId: 'line1',
			flipDirection: true,
			loop: true,
			scale: 0.03,
			positionScale: 1.8
		}]
	},

	areasSettings: {
		unlistedAreasColor: '#506580',
		unlistedAreasOutlineColor: '#232836'
	},

	imagesSettings: {
		color: '#2A93FC',
		rollOverColor: '#2A93FC',
		selectedColor: '#2A93FC',
		pauseDuration: 0.2,
		animationDuration: 2.5,
		adjustAnimationSpeed: true
	},

	linesSettings: {
		color: '#2A93FC',
		alpha: 1
	},

	export: {
		enabled:true
	}

});
});
