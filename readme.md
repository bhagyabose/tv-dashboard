# README #

### What is this repository for? ###

* UNHRD TV Dashboard

### How do I get set up? ###

* (sudo) composer update
* (sudo) npm install
* gulp
* php artisan serve
* open [http://localhost:8000](http://localhost:8000)

**CSS and JS files are inside resources/assets/sass and resources/assets/js**