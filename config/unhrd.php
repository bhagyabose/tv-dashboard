<?php

return [
    'orange_day' => 25,
    'orange_day_sentence' => "WFP Says NO Violence Against Women",
    'results_cache_duration' => 60,
    'refresh_interval' => 130,
    'depots' => [
		'AEHD' => ['country_code' => 'AE', 'name' => 'Dubai', "coordinates" => ["23.424076", "53.847818"]],
		'GHHD' => ['country_code' => 'GH', 'name' => 'Accra', "coordinates" => ["7.946527", "-1.023194"]],
		'MYHD' => ['country_code' => 'MY', 'name' => 'Kuala Lumpur', "coordinates" => ["4.210484", "101.975766"]],
		'PAHD' => ['country_code' => 'PA', 'name' => 'Panama City', "coordinates" => ["8.537981", "-80.782127"]],
		'ESHD' => ['country_code' => 'ES', 'name' => 'Las Palmas', "coordinates" => ["40.463667", "-3.749220"]],
		'ITHD' => ['country_code' => 'IT', 'name' => 'Brindisi', "coordinates" => ["41.871940", "12.567380"]],
		/*'DJHD' => ['country_code' => 'DJ', 'name' => 'Djibouti', "coordinates" => ["11.825138", "42.590275"]]*/
	],
    'page1' => [
        'title' => 'UNHRD Operations',
        'partner_bubble_unit' => 'mt',
        'date_format' => 'd F Y',
        'line_color' => '#2A93FC',
        'depot_country' => 'rgb(255, 255, 255)',//"rgba(75, 146, 219, 0.8)",
        'destination_country' => "rgba(200, 130, 153, 0.8)",
        'goods_per_sales_office' => "NFI Dispatched by Depot (Weight)",
        'goods_per_destination' => "NFI Heading to (Weight)",
        'top_cluster_items' => 'Top Cluster Items Moving',
        'partners_section' => 'Partners Served',
        'figures_box' => 'On The Move'
    ],
    'page2' => [
        'title' => 'Annual Overview 2016',
        'nfi_shipped_per_partner' => 'NFI Shipped per Partner',
        'nfi_shipped_per_hub' => 'NFI Shipped per Hub',
        'nfi_dispatched_total' => 'Business Overview',
        'nfi_dispatched_value' => 'Total NFI value (USD)',
        'nfi_dispatched_partners' => 'Partners',
        'loan_and_borrows' => 'Loans & Borrowings',
        'loan_and_borrows_total' => 'Value of NFI loaned to Partners (USD)',
        'loan_and_borrows_partners' => 'Number of Partners',
        'rrt' => 'Rapid Response Team',
        'rrt_deployment_days' => 'Deployment Days',
        'rrt_countries' => 'Countries',
        'rrt_trainings_held' => 'National Capacity Building - Trainings',
        'rrt_deployment_days_value' => '769',
        'rrt_countries_value' => '9',
        'rrt_trainings_held_value' => '5',
        'top_nfi_shipped' => 'Top Cluster Items Dispatched',
        'top_suppliers' => 'Top 5 Suppliers (NFI procured)',
        'total_weight' =>'8113',
        'total_volume' =>'56,890',
        'total_value' => '42,204,378',
        'shipments'=>'471',
        'countries'=>'82',
        'partners'=>'39'
    ],
    'page3' => [
        'title' => 'Ongoing Operations',
        'high_value_operations' => 'High Value Ongoing Operations (USD)',
        'high_value_po_goods' => 'High Value Open POs (Supplies)',
        'high_value_po_trans' => 'High Value Open POs (Transport)',
        'emergencies' => 'Emergencies Overview'
    ],
    'page5' => [
        'title' => 'Annual Overview - Logistics Reconciliation',
        'nfi_shipped_per_partner' => 'NFI Shipped per Partner',
        'nfi_shipped_per_hub' => 'NFI Shipped per Hub',
        'nfi_dispatched_total' => 'Annual Business Overview',
        'nfi_dispatched_value' => 'Total NFI value (USD)',
        'nfi_dispatched_partners' => 'Partners',
        'loan_and_borrows' => 'Loans & Borrowings',
        'loan_and_borrows_total' => 'Value of NFI loaned to Partners (USD)',
        'loan_and_borrows_partners' => 'Number of Partners',
        'top_nfi_shipped' => 'Top Cluster Items Dispatched',
        'top_suppliers' => 'Top 5 Suppliers (NFI procured)'
    ],
    'api' => [
        'line_color' => '#2A93FC',
        'depot_country' => 'rgb(0,0,0)',//"rgba(75, 146, 219, 0.8)",
        'destination_country' => "rgba(200, 130, 153, 0.8)",
    ]
];