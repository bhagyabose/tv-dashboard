var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

 elixir(function(mix) {
 	mix.sass('app.scss');
 	mix.sass('tv.scss');
 	mix.sass('admin.scss');
 });

 /*elixir(function(mix) {
 	mix.browserify('admin.js', null, null, {external: ['jquery']});
 	mix.browserify('tv.js', null, null, {external: ['jquery']});
});*/

 elixir(function(mix) {
 	mix.copy('node_modules/jquery-validation/dist/jquery.validate.js', 'public/js/jquery.validate.js');
 	mix.copy('node_modules/jquery.marquee/jquery.marquee.min.js', 'public/js/jquery.marquee.min.js');
 	mix.copy('node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js', 'public/js/bootstrap.min.js');
 	mix.copy('resources/assets/js/tv.js', 'public/js/tv.js');
 });