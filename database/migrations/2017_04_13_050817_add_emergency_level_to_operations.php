<?php

use Illuminate\Database\Migrations\Migration;

class AddEmergencyLevelToOperations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('operations', function ($table) {
            $table->string('level')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('operations', function ($table) {
            $table->dropColumn('level');
        });
    }
}
