<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipmentView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE VIEW shipments AS
            SELECT  pod_monitor.sales_orderpurchase_order,
                    pod_monitor.sales_office,
                    pod_monitor.name_of_sold_to_party as sold_to_party,
                    pod_monitor.name_of_the_ship_to_party,
                    pod_monitor.salespurchase_order_item_no,
                    pod_monitor.total_weight as weight,
                    pod_monitor.volume as volume,
                    pod_monitor.delivery_gi_value,
                    pod_monitor.act_gds_mvmnt_date,
                    pod_monitor.proof_of_delivery_date,
                    material.sector,
                    pod_monitor.country_key,
                    countries.name as country,
                    countries.latitude,
                    countries.longitude,
                    partners.logo as sold_to_party_logo,
                    partners.code as sold_to_party_code
            FROM pod_monitor
            LEFT JOIN material ON material.idMATERIAL = pod_monitor.material COLLATE utf8_general_ci
            LEFT JOIN countries on countries.twoLetterCode = pod_monitor.country_key COLLATE utf8_general_ci
            LEFT JOIN partners on partners.code = pod_monitor.sold_to_party COLLATE utf8_general_ci
            where pod_monitor.act_gds_mvmnt_date is not null
            and pod_monitor.material is not null;');
        /*DB::statement('CREATE VIEW current_shipments_totals AS SELECT results.sold_to_party, sum(results.weight), sum(results.volume), sum(results.delivery_gi_value) FROM current_shipments as results group by results.sold_to_party;');*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW shipments');
    }
}
