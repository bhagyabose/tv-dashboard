<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePodAnnualTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pod_annual', function (Blueprint $table) {
            $table->string('delivery', 255)->nullable();
            $table->string('sales_orderpurchase_order', 255)->nullable();
            $table->string('salespurchase_order_item_no', 255)->nullable();
            $table->string('pi_number', 255)->nullable();
            $table->date('act_gds_mvmnt_date')->nullable();
            $table->date('proof_of_delivery_date')->nullable();
            $table->string('sold_to_party', 255)->nullable();
            $table->string('name_of_sold_to_party', 255)->nullable();
            $table->string('name_of_the_ship_to_party', 255)->nullable();
            $table->string('country_key', 255)->nullable();
            $table->string('location_of_the_ship_to_party', 255)->nullable();
            $table->string('material', 255)->nullable();
            $table->string('description', 255)->nullable();
            $table->string('delivery_quantity', 255)->nullable();
            $table->string('sales_unit', 255)->nullable();
            $table->decimal('total_weight', 10, 2)->nullable();
            $table->string('weight_unit', 255)->nullable();
            $table->decimal('net_weight', 10, 2)->nullable();
            $table->decimal('volume', 10, 2)->nullable();
            $table->string('volume_unit', 255)->nullable();
            $table->decimal('delivery_gi_value', 10, 2)->nullable();
            $table->string('statistics_currency', 255)->nullable();
            $table->string('wbs_element', 255)->nullable();
            $table->string('description2', 255)->nullable();
            $table->string('description3', 255)->nullable();
            $table->string('plant', 255)->nullable();
            $table->string('sales_office', 255)->nullable();
            $table->string('incoterms', 255)->nullable();
            $table->string('incoterms_part_2', 255)->nullable();
            $table->string('geographical_area', 255)->nullable();
            $table->string('delivery_type', 255)->nullable();
            $table->string('item_category', 255)->nullable();
            $table->string('total_gds_mvt_stat', 255)->nullable();
            $table->string('goods_movement_stat', 255)->nullable();
            $table->string('overall_pickstatus', 255)->nullable();
            $table->string('overall_wm_status', 255)->nullable();
            $table->string('wm_activity_status', 255)->nullable();
            $table->string('shipping_type', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('pod_annual');
    }
}
