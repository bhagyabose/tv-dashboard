<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogisticsReconciliationReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logistics_reconciliation_report', function (Blueprint $table) {
            $table->string('so_type', 4)->nullable();
            $table->date('so_date');
            $table->string('sales_order', 255);
            $table->string('so_item', 255)->nullable();
            $table->string('material',255)->nullable();
            $table->decimal('item_quantity', 10, 2)->nullable();
            $table->string('so_item_description',255)->nullable();
            $table->string('sales_office', 4)->nullable();
            $table->string('sold_to_party', 255)->nullable();
            $table->string('sold_to_party_name', 255)->nullable();
            $table->string('ship_to_party', 255)->nullable();
            $table->string('ship_to_party_name', 255)->nullable();
            $table->decimal('actual_cost_usd', 10, 2)->nullable();
            $table->decimal('mrc_value_usd', 10, 2)->nullable();
            $table->string('order_reason', 255)->nullable();
            $table->string('rejectionreason', 255)->nullable();
            $table->date('created_on')->nullable();
            $table->string('created_by', 255)->nullable();
            $table->string('wbs_number', 255)->nullable();
            $table->string('wbs_description', 255)->nullable();
            $table->string('pi_number', 255)->nullable();
            $table->decimal('pi_value', 10,2)->nullable();
            $table->date('pi_date')->nullable();
            $table->string('pi_status', 255)->nullable();
            $table->string('debit_note_draft', 255)->nullable();
            $table->decimal('dnd_value', 10, 2)->nullable();
            $table->date('dnd_date')->nullable();
            $table->string('dnd_status', 255)->nullable();
            $table->string('debit_note_reld', 255)->nullable();
            $table->decimal('dnr_value', 10,2)->nullable();
            $table->date('dnr_date')->nullable();
            $table->string('dnr_status', 255)->nullable();
            $table->string('int_po_number', 255)->nullable();
            $table->string('int_po_item', 255)->nullable();
            $table->string('vendor', 255)->nullable();
            $table->decimal('int_po_value', 10,2)->nullable();
            $table->string('int_po_rel_status', 255)->nullable();
            $table->date('int_po_rel_date')->nullable();
            $table->string('delivery_number', 255)->nullable();
            $table->decimal('del_tot_weight', 10,2)->nullable();
            $table->decimal('del_tot_volume', 10,2)->nullable();
            $table->string('volume_unit', 255)->nullable();
            $table->decimal('del_tot_value', 10,2)->nullable();
            $table->string('delivery_status', 255)->nullable();
            $table->string('proof_of_del_status', 255)->nullable();
            $table->string('pr_number', 255)->nullable();
            $table->string('pr_item', 255)->nullable();
            $table->string('pr_release_status', 255)->nullable();
            $table->date('pr_release_date')->nullable();
            $table->string('ext_po_number', 255)->nullable();
            $table->string('ext_po_item', 255)->nullable();
            $table->string('ext_po_rel_status', 255)->nullable();
            $table->date('ext_po_rel_date')->nullable();
            $table->string('po_currency', 255)->nullable();
            $table->string('report_currency', 255)->nullable();
            $table->string('po_item_val', 255)->nullable();
            $table->string('ses_number', 255)->nullable();
            $table->string('ses_item', 255)->nullable();
            $table->string('ses_item_qty', 255)->nullable();
            $table->decimal('ses_item_value', 10,2)->nullable();
            $table->string('grn', 255)->nullable();
            $table->string('grn_item', 255)->nullable();
            $table->string('grn_qty', 255)->nullable();
            $table->decimal('grn_value', 10,2)->nullable();
            $table->string('liv', 255)->nullable();
            $table->string('liv_item', 255)->nullable();
            $table->string('liv_qty', 255)->nullable();
            $table->decimal('liv_value', 10,2)->nullable();
            $table->string('subsqt_drcr', 255)->nullable();
            $table->string('subsqt_item', 255)->nullable();
            $table->string('sbsqt_item_qty', 255)->nullable();
            $table->decimal('sbsqt_item_value', 10,2)->nullable();
            $table->string('incoterms_2', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('logistics_reconciliation_report');
    }
}
