<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('po_report', function (Blueprint $table) {
            $table->string('po_number', 255)->nullable();
            $table->string('item', 255)->nullable();
            $table->date('po_date', 255)->nullable();
            $table->date('po_rel_date', 255)->nullable();
            $table->string('po_releasor', 255)->nullable();
            $table->string('porg', 255)->nullable();
            $table->string('pgr', 255)->nullable();
            $table->string('creator', 255)->nullable();
            $table->string('type', 255)->nullable();
            $table->string('vendor', 255)->nullable();
            $table->string('vendor_name', 255)->nullable();
            $table->string('vendor_country_code', 255)->nullable();
            $table->string('vendor_country', 255)->nullable();
            $table->string('recipient_country', 255)->nullable();
            $table->string('mat_group', 255)->nullable();
            $table->string('mat_group_desc', 255)->nullable();
            $table->string('mat_grp_detail', 255)->nullable();
            $table->string('description', 255)->nullable();
            $table->decimal('quantity', 10, 2)->nullable();
            $table->decimal('uom', 10, 2)->nullable();
            $table->decimal('unit_price', 10, 2)->nullable();
            $table->decimal('po_val_loc', 10, 2)->nullable();
            $table->string('curr', 255)->nullable();
            $table->string('value_us', 255)->nullable();
            $table->date('del_date', 255)->nullable();
            $table->string('dc', 255)->nullable();
            $table->string('d', 255)->nullable();
            $table->string('inco', 255)->nullable();
            $table->string('pr_no', 255)->nullable();
            $table->decimal('itm', 10, 2)->nullable();
            $table->string('coll_no', 255)->nullable();
            $table->string('lta_no', 255)->nullable();
            $table->decimal('gr_value', 10, 2)->nullable();
            $table->decimal('liv_value', 10, 2)->nullable();
            $table->decimal('po_gr_value', 10, 2)->nullable();
            $table->decimal('gr_liv_value', 10, 2)->nullable();
            $table->string('ba', 255)->nullable();
            $table->string('fa', 255)->nullable();
            $table->string('grant', 255)->nullable();
            $table->string('fund', 255)->nullable();
            $table->string('ccntr', 255)->nullable();
            $table->string('order', 255)->nullable();
            $table->string('wbs_element', 255)->nullable();
            $table->string('asset', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('po_report');
    }
}
