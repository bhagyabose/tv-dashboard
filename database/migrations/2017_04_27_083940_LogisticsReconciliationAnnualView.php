<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LogisticsReconciliationAnnualView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      DB::statement('CREATE VIEW logistics_annual AS
            SELECT  logistics_reconciliation_report.sales_order,
                    logistics_reconciliation_report.sales_office,
                    logistics_reconciliation_report.sold_to_party_name as sold_to_party,
                    logistics_reconciliation_report.so_item_description,
                    logistics_reconciliation_report.del_tot_weight as weight,
                    logistics_reconciliation_report.del_tot_volume as volume,
                    logistics_reconciliation_report.actual_cost_usd,
                    logistics_reconciliation_report.so_date,
                    material.sector,
                    partners.logo as sold_to_party_logo,
                    partners.code as sold_to_party_code
            FROM logistics_reconciliation_report
            LEFT JOIN material ON material.idMATERIAL = logistics_reconciliation_report.material COLLATE utf8_general_ci
            LEFT JOIN partners on partners.code = logistics_reconciliation_report.sold_to_party COLLATE utf8_general_ci
            where logistics_reconciliation_report.liv_value is not null
            and logistics_reconciliation_report.material is not null;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         DB::statement('DROP VIEW logistics_annual');
    }
}
