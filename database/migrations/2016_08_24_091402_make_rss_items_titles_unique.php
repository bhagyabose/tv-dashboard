<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeRssItemsTitlesUnique extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rss_items', function ($table) {
            $table->unique('text');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // here it should remove the uniqueness of the "text" field
        /*Schema::table('rss_items', function ($table) {
            $table->dropUnique('text');
        });*/
    }
}
