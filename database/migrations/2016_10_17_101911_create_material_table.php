<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material', function (Blueprint $table) {
            $table->string('idMATERIAL', 255);
            $table->string('CLUSTER_DESCRIPTION', 255)->nullable();
            $table->string('DESCRIPTION', 255);
            $table->string('SECTOR', 255);
            $table->string('CATEGORY', 255);
            $table->string('FAMILY', 255);
            $table->primary('idMATERIAL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material');
    }
}
