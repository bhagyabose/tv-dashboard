<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnualShipmentsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE VIEW shipments_annual AS
            SELECT  pod_annual.sales_orderpurchase_order,
                    pod_annual.sales_office,
                    pod_annual.name_of_sold_to_party as sold_to_party,
                    pod_annual.name_of_the_ship_to_party,
                    pod_annual.salespurchase_order_item_no,
                    pod_annual.total_weight as weight,
                    pod_annual.volume as volume,
                    pod_annual.delivery_gi_value,
                    pod_annual.act_gds_mvmnt_date,
                    pod_annual.proof_of_delivery_date,
                    material.sector,
                    pod_annual.country_key,
                    countries.name as country,
                    countries.latitude,
                    countries.longitude,
                    partners.logo as sold_to_party_logo,
                    partners.code as sold_to_party_code
            FROM pod_annual
            LEFT JOIN material ON material.idMATERIAL = pod_annual.material COLLATE utf8_general_ci
            LEFT JOIN countries on countries.twoLetterCode = pod_annual.country_key COLLATE utf8_general_ci
            LEFT JOIN partners on partners.code = pod_annual.sold_to_party COLLATE utf8_general_ci
            where pod_annual.act_gds_mvmnt_date is not null
            and pod_annual.material is not null;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW shipments_annual');
    }
}
