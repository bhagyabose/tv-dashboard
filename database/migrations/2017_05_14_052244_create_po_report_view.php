<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoReportView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         DB::statement('CREATE VIEW highValuePO AS
            SELECT  po_report.po_number,
                    po_report.type,
                    material.sector,
                    po_report.mat_group,
                    po_report.mat_group_desc,
                    po_report.mat_grp_detail,
                    po_report.description,
                    po_report.value_us,
                    po_report.liv_value,
                    po_report.gr_value,
                    po_report.creator,
                    po_report.recipient_country
            FROM po_report
            LEFT JOIN material ON material.DESCRIPTION = upper(po_report.description) COLLATE utf8_general_ci');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW highValuePO');
    }
}
