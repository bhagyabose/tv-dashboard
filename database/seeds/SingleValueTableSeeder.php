<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

use Dashboard\Models\SingleValue as SingleValue;

class SingleValueTableSeeder extends Seeder {

	public function run()
	{
		SingleValue::create(["key" => "total_loans", "value" => "0"]);
		SingleValue::create(["key" => "total_loaned_partners", "value" => "0"]);
	}

}