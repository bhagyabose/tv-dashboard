<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LoadCountryData::class);
        $this->call(LoadMaterialData::class);
        $this->call(LoadPartnerData::class);
        $this->call(SingleValueTableSeeder::class);
    }
}
