<?php

use Illuminate\Database\Seeder;

use Dashboard\Models\Partner as Partner;

class LoadPartnerData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Partner::create(["code" => "2000087", "name" => "INTERSOS"]);
        Partner::create(["code" => "2000324", "name" => "WORLD HEALTH ORGANIZATION"]);
        Partner::create(["code" => "2000332", "name" => "FOOD AND AGRICULTURE ORGANISATION O"]);
        Partner::create(["code" => "2000411", "name" => "World Vision International", "logo" => "images/partners/2000411.png"]);
        Partner::create(["code" => "2000414", "name" => "UN OCHA - GENEVA"]);
        Partner::create(["code" => "2000414", "name" => "United Nations Office for the Coord"]);
        Partner::create(["code" => "2000535", "name" => "UNICEF SUPPLY DIVISION"]);
        Partner::create(["code" => "2000588", "name" => "ACTION CONTRE LA FAIM"]);
        Partner::create(["code" => "2000631", "name" => "USAID-Agency for International Deve"]);
        Partner::create(["code" => "2000784", "name" => "WORLD HEALTH ORGANIZATION (WHO)", "logo" => "images/partners/2000784.svg"]);
        Partner::create(["code" => "2000817", "name" => "Irish Aid", "logo" => "images/partners/2000817.svg"]);
        Partner::create(["code" => "2001020", "name" => "Action Contre La Faim"]);
        Partner::create(["code" => "2001056", "name" => "UNDP NY HQ"]);
        Partner::create(["code" => "2001067", "name" => "Cooperazione Italiana allo Sviluppo"]);
        Partner::create(["code" => "2001083", "name" => "UN High Commissioner for Refugees U", "logo" => "images/partners/2001083.png"]);
        Partner::create(["code" => "2001102", "name" => "FAO"]);
        Partner::create(["code" => "2001232", "name" => "PAN AMERICAN HEALTH ORGANIZATION /"]);
        Partner::create(["code" => "2001242", "name" => "CARE USA"]);
        Partner::create(["code" => "2001272", "name" => "THE SWISS RED CROSS"]);
        Partner::create(["code" => "2001422", "name" => "Handicap International Federation"]);
        Partner::create(["code" => "2001483", "name" => "SAVE THE CHILDREN INTERNATIONAL", "logo" => "images/partners/2001483.png"]);
        Partner::create(["code" => "2001515", "name" => "MERCY MALAYSIA"]);
        Partner::create(["code" => "2001530", "name" => "CATHOLIC RELIEF SERVICES-USCCB"]);
        Partner::create(["code" => "2001531", "name" => "the international rescue committee"]);
        Partner::create(["code" => "2001532", "name" => "INTERNATIONAL FEDERATION OF RED CRO"]);
        Partner::create(["code" => "2002047", "name" => "Department of Foreign Affairs and T"]);
        Partner::create(["code" => "2002102", "name" => "OXFAM INTERMON"]);
        Partner::create(["code" => "2002107", "name" => "Fundacion Accion Contra El Hambre"]);
        Partner::create(["code" => "2002288", "name" => "COMISION CASCOS BLANCOS ARGENTINA"]);
        Partner::create(["code" => "2002291", "name" => "OFICINA TECNICA DE COOPERACION EN P"]);
        Partner::create(["code" => "2002328", "name" => "SHELTER BOX", "logo" => "images/partners/2002328.svg"]);
        Partner::create(["code" => "2002357", "name" => "UNITED NATIONS RELIEF AND WORKS AGE"]);
        Partner::create(["code" => "2002433", "name" => "ASEAN Coordinating Centre for Human"]);
        Partner::create(["code" => "2002490", "name" => "FRANCE DIPLOMATIE MINISTERE DES AFF"]);
        Partner::create(["code" => "2002562", "name" => "SOLIDARITES INTERNATIONAL"]);
        Partner::create(["code" => "2002767", "name" => "MEDAIR"]);
        Partner::create(["code" => "2002803", "name" => "Lutheran World Relief"]);
        Partner::create(["code" => "2003672", "name" => "Adventist Development and Relief Ag"]);
        Partner::create(["code" => "2003784", "name" => "Save the Children UK"]);
        Partner::create(["code" => "2004029", "name" => "World Health Organization"]);
        Partner::create(["code" => "2004347", "name" => "MINISTERIO LA PRESIDENCIA"]);
        Partner::create(["code" => "2004872", "name" => "FEDERAL MINISTRY OF THE INTERIOR OF"]);
        Partner::create(["code" => "6000005", "name" => "CO - Cameroon"]);
        Partner::create(["code" => "6000006", "name" => "CO - Chad"]);
        Partner::create(["code" => "6000014", "name" => "CO - Congo D.R."]);
        Partner::create(["code" => "6000015", "name" => "CO - Central African Republic"]);
        Partner::create(["code" => "6000017", "name" => "CO - COTE D'IVOIRE"]);
        Partner::create(["code" => "6000023", "name" => "CO - Ecuador"]);
        Partner::create(["code" => "6000026", "name" => "CO - Ethiopia"]);
        Partner::create(["code" => "6000036", "name" => "CO - Haiti"]);
        Partner::create(["code" => "6000037", "name" => "CO - Indonesia"]);
        Partner::create(["code" => "6000039", "name" => "CO - Iraq"]);
        Partner::create(["code" => "6000041", "name" => "CO - Jordan"]);
        Partner::create(["code" => "6000052", "name" => "CO - Madagascar"]);
        Partner::create(["code" => "6000054", "name" => "CO - Mali"]);
        Partner::create(["code" => "6000057", "name" => "CO - Niger"]);
        Partner::create(["code" => "6000059", "name" => "CO - Nepal"]);
        Partner::create(["code" => "6000060", "name" => "CO - Peru"]);
        Partner::create(["code" => "6000062", "name" => "CO - Pakistan"]);
        Partner::create(["code" => "6000067", "name" => "CO - Sudan"]);
        Partner::create(["code" => "6000068", "name" => "CO - Sierra Leone"]);
        Partner::create(["code" => "6000070", "name" => "CO - Somalia"]);
        Partner::create(["code" => "6000076", "name" => "CO - Tanzania"]);
        Partner::create(["code" => "6000077", "name" => "CO - Uganda"]);
        Partner::create(["code" => "6000081", "name" => "CO - Zimbabwe"]);
        Partner::create(["code" => "6000083", "name" => "CO - Malawi"]);
        Partner::create(["code" => "6000098", "name" => "RB - LATIN AMERICA & CARIBBEAN"]);
        Partner::create(["code" => "6000099", "name" => "RB - WEST AFRICA (DAKAR)"]);
        Partner::create(["code" => "6000100", "name" => "CO - SWAZILAND"]);
        Partner::create(["code" => "6000101", "name" => "RB - ASIA BUREAU (BANGKOK)"]);
        Partner::create(["code" => "6000107", "name" => "RB - East centr & South Africa"]);
        Partner::create(["code" => "6000122", "name" => "LO - Japan Liaison Office"]);
        Partner::create(["code" => "6000123", "name" => "CO - MYANMAR"]);
        Partner::create(["code" => "6000126", "name" => "WFP HQ"]);
        Partner::create(["code" => "6000145", "name" => "CO - SOUTH SUDAN"]);
        Partner::create(["code" => "6000155", "name" => "WFP PARAGUAY CO"]);
        Partner::create(["code" => "6000190", "name" => "CO – Ukraine"]);
    }
}
