<?php

namespace Dashboard\Http\Controllers;

use Illuminate\Http\Request;
use Dashboard\Http\Requests;

use Dashboard\Models\RSSItem;

use SimplePie;

class RSSItemController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$rss_items = RSSItem::orderBy('id', 'desc')->paginate(20);

		return view('rss_items.index', compact('rss_items'));
	}

	/**
	 * Import RSS items from the UNHRD RSS feed
	 *
	 * @return Response
	 */
	public function import(Request $request)
	{
		//$feed_url = 'http://unhrd.org/news.xml';
		$feed_url = 'http://www.gdacs.org/xml/rss.xml';

		$simplepie = new SimplePie();
//		$simplepie->set_feed_url($feed_url);
		$simplepie->set_raw_data(preg_replace(array('/\s{2,}/', '/[\t\n]/'), '', file_get_contents($feed_url)));
		$simplepie->init();
		$simplepie->handle_content_type();

		foreach ($simplepie->get_items() as $item) {
			try {
				$rss_item = new RSSItem;
				$rss_item->text = $item->get_title();
				$rss_item->visible = true;
				$rss_item->posted_date = $item->get_gmdate('Y-m-d H:i');
				$rss_item->save();
			} catch(\Illuminate\Database\QueryException $e) {
				$request->session()->flash('error', "Some duplicates could not be imported");
			}
		}

		$request->session()->flash('status', "Finished importing RSS Items");
		return redirect()->action('RSSItemController@index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		return view('rss_items.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$rss_item = new RSSItem();

		$rss_item->text = $request->input("text");
		$rss_item->visible = $request->input("visible");
		$rss_item->expiration_date = $request->input("expiration_date");

		$rss_item->save();

		return redirect()->route('admin.rss_items.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$rss_item = RSSItem::findOrFail($id);

		return view('rss_items.show', compact('rss_item'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$rss_item = RSSItem::findOrFail($id);

		return view('rss_items.edit', compact('rss_item'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$rss_item = RSSItem::findOrFail($id);

		$rss_item->text = $request->input("text");
		$rss_item->visible = $request->input("visible");
		$rss_item->expiration_date = $request->input("expiration_date");

		$rss_item->save();

		return redirect()->route('admin.rss_items.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$rss_item = RSSItem::findOrFail($id);
		$rss_item->delete();

		return redirect()->route('admin.rss_items.index')->with('message', 'Item deleted successfully.');
	}

}
