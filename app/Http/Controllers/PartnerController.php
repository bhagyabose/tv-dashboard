<?php

namespace Dashboard\Http\Controllers;

use Dashboard\Http\Requests;
use Dashboard\Http\Controllers\Controller;

use Dashboard\Models\Partner;
use Illuminate\Http\Request;

class PartnerController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$partners = Partner::orderBy('name')->paginate(20);

		return view('partners.index', compact('partners'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('partners.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$partner = new Partner();

		$partner->code = $request->input("code");
                $partner->name = $request->input("name");

                $imageName = $partner->code . '.' . $request->file('logo')->getClientOriginalExtension();
                $imageDestination = public_path("images/partners");
                $request->file('logo')->move($imageDestination, $imageName);
                $partner->logo = 'images/partners/'.$imageName;

		$partner->save();

		return redirect()->route('admin.partners.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$partner = Partner::findOrFail($id);

		return view('partners.show', compact('partner'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$partner = Partner::findOrFail($id);

		return view('partners.edit', compact('partner'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$partner = Partner::findOrFail($id);

		$partner->code = $request->input("code");
                $partner->name = $request->input("name");

                $imageName = $partner->code . '.' . $request->file('logo')->getClientOriginalExtension();
                $imageDestination = public_path("images/partners");
                $request->file('logo')->move($imageDestination, $imageName);
                $partner->logo = 'images/partners/'.$imageName;

		$partner->save();

		return redirect()->route('admin.partners.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$partner = Partner::findOrFail($id);
		$partner->delete();

		return redirect()->route('admin.partners.index')->with('message', 'Item deleted successfully.');
	}

}
