<?php

namespace Dashboard\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\File as File;
use Illuminate\Support\Facades\Input as Input;

class PhotoController extends Controller
{
    public function snap_shots() {
		return view('snap_shots.image');
	}
        
    public function upload(Request $request) {
              $destinationPath = './uploads'; // upload path
              $filenames = File::files($destinationPath);
              $file_count = count($filenames);
              if($file_count >=6){
                // While there are six or more files.
                while ( $file_count  > 5 ) {
                    $oldest_file = null;
                    // Grab the unix timestamp of *now*, as filemtime returns a unix timestamp too.
                    $current_oldest_time = time();

                    foreach ( $filenames as $filename ) {
                        $filetime = filectime( $filename );
                        if ( $filetime < $current_oldest_time ) {
                            $oldest_file = $filename;
                        }
//                        dd($filenames);
                    }
                    // After the foreach you'll have the filename of the oldest file (remove it),:
                        unlink( $oldest_file );
                    // Or null if something went wrong (break out of the loop):
                    break;
                }
              }
              //  Upload file
              $extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
              $fileName = rand(11111,99999).'.'.$extension; // renameing image
              Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
              $request->session()->flash('status', "Image Uploaded !");
              return redirect()->action('PhotoController@snap_shots');
        }

    
}
