<?php

namespace Dashboard\Http\Controllers;

use Illuminate\Http\Request;
use Cache;
use Dashboard\Http\Requests;
use Dashboard\Models\CurrentShipmentTotals as CurrentShipmentTotals;

class APIController extends Controller
{
	public function mapData()
	{
		$countryPairs = Cache::remember('shipmentCountryPairs', config('unhrd.results_cache_duration'), function() {
			return CurrentShipmentTotals::shipmentCountryPairs()->get();
		});

		$countryPairsArray = [];
		$coordinatesPairsArray = [];
		$i = 0;
		foreach ($countryPairs as $pair) {
			// this variable is used so lines can have an ID so I can show the airplane on them
			$i++;
			$countryPairsArray[] = [salesOfficeCountryCode($pair->sales_office), $pair->country_key];
			$coordinatesPairsArray[] = [salesOfficeCoordinates($pair->sales_office), [$pair->latitude, $pair->longitude], "line".$i];
		}

		return response()->view('api.map-data', ['mapData' => array('pairs' => $countryPairsArray, 'coordinates' => $coordinatesPairsArray, 'depots' => config('unhrd.depots'))])->header('Content-Type', 'application/json');;
	}
}
