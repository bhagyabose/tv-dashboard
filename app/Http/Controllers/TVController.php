<?php

namespace Dashboard\Http\Controllers;

use DB;
use Cache;
//use Debugbar;
use Illuminate\Http\Request;
use Dashboard\Http\Requests;
use Illuminate\Support\Facades\File as File;
use Dashboard\Models\CurrentShipmentTotals as CurrentShipmentTotals;
use Dashboard\Models\ShipmentsAnnual as Shipments;
use Dashboard\Models\POReportLine as POReportLine;
use Dashboard\Models\HighValuePO as HighValuePO;
use Dashboard\Models\SingleValue as SingleValue;
use Dashboard\Models\Operation as Operation;
use Dashboard\Models\LCRLine as LCRLine;
use Dashboard\Models\LogisticsAnnual as Annual;
use Dashboard\Models\Charts\ColumnChart as ColumnChart;
use Dashboard\Models\Charts\PieChart as PieChart;

use Dashboard\Models\Charts\ProgressBar as ProgressBar;
use Dashboard\Models\Charts\IconPanel as IconPanel;
use Dashboard\Models\Charts\POTable as POTable;
use Dashboard\Models\Charts\OverviewTable as OverviewTable;

use Dashboard\Models\RSSItem;

use Carbon\Carbon as Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TVController extends Controller
{
	public function page1(Request $request)
	{
		$goodsPerSalesOfficeChart = $this->getGoodsPerSalesOfficeChart();
		$goodsPerDestinationChart = $this->getGoodsPerDestinationChart();
		$topItemsPerSector = $this->getTopItemsPerSector();
		$goodsPerPartner = $this->getTopPartners();
		$currentFigures = $this->getCurrentFigures();
		$rssItemsString = $this->getRSSItemsString();
		$mapData = $this->getDataForMap();

		return view('tv.page1', ['date' => \Carbon\Carbon::now()->format(config('unhrd.page1.date_format')),
			'goodsPerSalesOfficeChart' => $goodsPerSalesOfficeChart,
			'goodsPerDestinationChart' => $goodsPerDestinationChart,
			'topItemsPerSector' => $topItemsPerSector,
			'goodsPerPartner' => $goodsPerPartner,
			'currentFigures' => $currentFigures,
			'rssItemsString' => $rssItemsString,
			'mapData' => $mapData]);
	}

	public function page2(Request $request)
	{
		$rssItemsString = $this->getRSSItemsString();
		
		$timeFrame = Carbon::now()->subYear()->format('F Y')." - ".Carbon::now()->format('F Y');

		$nfiShippedPerPartner = $this->nfiShippedPerPartner();
		$nfiShippedPerHub = $this->nfiShippedPerHub();
		$nfiTopFiveSuppliers = $this->nfiTopFiveSuppliers();
		$nfiTopFiveShipped = $this->nfiTopFiveShipped();
		
		$annualFigures = $this->getAnnualFigures();

		return view('tv.page2', ['rssItemsString' => $rssItemsString,
			'timeFrame' => $timeFrame,
			'nfiShippedPerPartner' => $nfiShippedPerPartner,
			'nfiShippedPerHub' => $nfiShippedPerHub,
			'nfiTopFiveSuppliers' => $nfiTopFiveSuppliers,
			'nfiTopFiveShipped' => $nfiTopFiveShipped,
			'annualFigures' => $annualFigures]);
	}

	public function page3(Request $request)
	{
		$rssItemsString = $this->getRSSItemsString();
		$highValueOperations = $this->highValueOperations();
		$highValueOpenPOsGoods = $this->highValueOpenPOsGoods();
		$highValueOpenPOsTransport = $this->highValueOpenPOsTransport();
		$overviewTable = $this->overviewTable();
                $imagePath = $this->imagePath();
		return view('tv.page3', ['rssItemsString' => $rssItemsString,
                                         'highValueOperations' => $highValueOperations,
                                          'highValueOpenPOsGoods' => $highValueOpenPOsGoods,
                                          'highValueOpenPOsTransport' => $highValueOpenPOsTransport,
                                          'overviewTable' => $overviewTable,
                                          'imagePath' => $imagePath]);
	}
        
        public function page4(Request $request)
        {
            $rssItemsString = $this->getRSSItemsString();
            return view('tv.page4',['rssItemsString' => $rssItemsString]);
        }
        
        public function page5(Request $request)
	{
		$rssItemsString = $this->getRSSItemsString();
		
		$timeFrame = Carbon::now()->subYear()->format('F Y')." - ".Carbon::now()->format('F Y');

		$nfiShippedPerPartner = $this->nfiShippedPerPartners();
		$nfiShippedPerHub = $this->nfiShippedPerDepot();
		$nfiTopFiveSuppliers = $this->nfiTopFiveSupplier();
		$nfiTopFiveShipped = $this->nfiTopFivesShipped();
		
		$annualFigures = $this->getAnnualFigure();

		return view('tv.page5', ['rssItemsString' => $rssItemsString,
			'timeFrame' => $timeFrame,
			'nfiShippedPerPartner' => $nfiShippedPerPartner,
			'nfiShippedPerHub' => $nfiShippedPerHub,
			'nfiTopFiveSuppliers' => $nfiTopFiveSuppliers,
			'nfiTopFiveShipped' => $nfiTopFiveShipped,
			'annualFigures' => $annualFigures]);
	}
        
	private function getRSSItemsString()
	{
		$rssItems = RSSItem::where('visible', 1)->orderBy('posted_date', 'desc')->limit(10)->get()->toArray();
		$func = function ($item) { return $item['text']; };
		return join(" | ", array_map($func, $rssItems));
	}

	//MARK: - Page 1
	private function getCurrentFigures()
	{
		// TODO get actual values
		/*$totals = Cache::remember('totals', config('unhrd.results_cache_duration'), function() {
			return CurrentShipmentTotals::totals()->get()->first();
		});*/
		$totals = CurrentShipmentTotals::totals()->get()->first();

		$perDestinationCount = Cache::remember('page1.perDestinationCount', config('unhrd.results_cache_duration'), function() {
			return CurrentShipmentTotals::perDestination()->currentValue()->get()->count();
		});

		$totalShipments = Cache::remember('page1.totalShipments', config('unhrd.results_cache_duration'), function() {
			return CurrentShipmentTotals::select('sales_orderpurchase_order')->distinct()->get()->count();
		});

		$totalPartners = Cache::remember('page1.totalPartners', config('unhrd.results_cache_duration'), function() {
			return CurrentShipmentTotals::perSoldToParty()->get();
		});
		$currentFigures = array();
		$currentFigures['totalWeight'] = number_format($totals->weight, 0, ".", ",");
		$currentFigures['totalVolume'] = number_format($totals->volume, 0, ".", ",");
		$currentFigures['totalValue'] = number_format($totals->value, 0, ".", ",");
		$currentFigures['countriesReached'] = $perDestinationCount;
		$currentFigures['shipments'] = $totalShipments;
		$currentFigures['totalPartners'] = count($totalPartners);

		return $currentFigures;
	}

	private function getDataForMap()
	{
		$countryPairs = Cache::remember('page1.shipmentCountryPairs', config('unhrd.results_cache_duration'), function() {
			return CurrentShipmentTotals::shipmentCountryPairs()->get();
		});

		$countryPairsArray = [];
		$coordinatesPairsArray = [];
		$i = 0;
		foreach ($countryPairs as $pair) {
			// this variable is used so lines can have an ID so I can show the airplane on them
			$i++;
			$countryPairsArray[] = [salesOfficeCountryCode($pair->sales_office), $pair->country_key];
			$coordinatesPairsArray[] = [salesOfficeCoordinates($pair->sales_office), [$pair->latitude, $pair->longitude], "line".$i];
		}

		return array('pairs' => $countryPairsArray, 'coordinates' => $coordinatesPairsArray, 'depots' => config('unhrd.depots'));
	}

	private function getGoodsPerSalesOfficeChart()
	{
		$goodsPerSalesOffice = Cache::remember('page1.perSalesOffice', config('unhrd.results_cache_duration'), function() {
			return CurrentShipmentTotals::perSalesOffice()->orderBy('weight', 'desc')->get();
		}); 
		$goodsPerSalesOfficeChartData = [];
		foreach ($goodsPerSalesOffice as $item) {
                    if($item->weight >0){
			$goodsPerSalesOfficeChartData[] = ['label' => salesOfficeForCode($item->sales_office),
                                    'value' => $item->weight < 1 ? $item->weight : round($item->weight)];
                    }
		}
		$goodsPerSalesOfficeChart = new ColumnChart(
			config('unhrd.page1.goods_per_sales_office'),
			$goodsPerSalesOfficeChartData,
			"#2A93FC",
			"column2"
			);

		return $goodsPerSalesOfficeChart;
	}

	private function getGoodsPerDestinationChart()
	{
		$goodsPerDestination = Cache::remember('page1.perDestination', config('unhrd.results_cache_duration'), function() {
			return CurrentShipmentTotals::perDestination()->orderBy('weight', 'desc')->take(6)->get();
		});

		$goodsPerDestinationChartData = [];
		foreach ($goodsPerDestination as $item) {
                    if($item->weight >0){
			$goodsPerDestinationChartData[] = ['label' => $item->country,
				'value' => round($item->weight)];
                    }
		}

		$goodsPerDestinationChart = new ColumnChart(
			config('unhrd.page1.goods_per_destination'),
			$goodsPerDestinationChartData,
			"#00BFA5",
			"column1"
			);

		return $goodsPerDestinationChart;
	}

	private function getTopItemsPerSector()
	{
		$goodsPerSector = Cache::remember('page1.topItems.perSector', config('unhrd.results_cache_duration'), function() {
			return CurrentShipmentTotals::perSector()->orderBy('weight', 'desc')->take(4)->get();
		});

		$total = Cache::remember('page1.topItems.totals', config('unhrd.results_cache_duration'), function() {
			return CurrentShipmentTotals::totals()->sum('weight');
		});

		$topItemsPerSector = [];
		$i = 0;
		foreach ($goodsPerSector as $item) {
			$i++;
                        $value = round($item->weight/$total, 2)*100;
			$topItemsPerSector[] = new PieChart(
				title_case_with_exceptions($item->sector),
				$value,
				"#D62331",
				"pie".$i,
                                '%'
				);
		}

		return $topItemsPerSector;
	}

	private function getTopPartners()
	{
		$goodsPerPartner = Cache::remember('page1.perPartner', config('unhrd.results_cache_duration'), function() {
//			return CurrentShipmentTotals::perSoldToParty()->orderBy('weight', 'desc')->take(8)->get();
                    return CurrentShipmentTotals::perSoldToParty()->orderBy('weight', 'desc')->get();
		});
//        dd($goodsPerPartner);
                $code = 0;
		$topItemsPerPartner = [];
		foreach ($goodsPerPartner as $item) {
			$topItemsPerPartner[] = ['logo' => asset($item->logo),
                                                 'weight' => ($item->weight < 1 ? round($item->weight, 2) : round($item->weight)),
                                                 'name' => $item->sold_to_party];
		}
//                dd($topItemsPerPartner);
		return $topItemsPerPartner;
	}

	//MARK: - Page 2
	private function getAnnualFigures()
	{
		$totals = Shipments::totals()->get()->first();
//                $totals = Shipments::totals()->get();
		$perDestinationCount = Cache::remember('page2.perDestinationCount', config('unhrd.results_cache_duration'), function() {
			return Shipments::perDestination()->get()->count();
		});

		$totalShipments = Cache::remember('page2.totalShipments', config('unhrd.results_cache_duration'), function() {
			return Shipments::select('sales_orderpurchase_order')->distinct()->get()->count();
		});

		$totalPartners = Cache::remember('page2.totalPartners', config('unhrd.results_cache_duration'), function() {
			return Shipments::perSoldToParty()->get();
		});

		$annualFigures = array();
                
		$annualFigures['annualTotalWeight'] = number_format($totals->weight, 0, ".", ",");
		$annualFigures['annualTotalVolume'] = number_format($totals->volume, 0, ".", ",");
		$annualFigures['annualTotalValue'] = number_format($totals->value, 0, ".", ",");
		$annualFigures['annualCountriesReached'] = $perDestinationCount;
		$annualFigures['annualShipments'] = $totalShipments;
		$annualFigures['annualTotalPartners'] = count($totalPartners);

		$annualFigures['totalLoans'] = SingleValue::find('total_loans')->value;
		$annualFigures['totalLoanedPartners'] = SingleValue::find('total_loaned_partners')->value;

		return $annualFigures;
	}

	private function nfiShippedPerHub()
	{
		$goodsPerSalesOffice = Cache::remember('page2.nfiShippedPerHub.each', config('unhrd.results_cache_duration'), function() {
                    return Shipments::perSalesOffice()->get();
		});
                $sum =0;
                foreach ($goodsPerSalesOffice as $item) {
                    $sum = $sum + $item->weight;
                }
		$nfiShippedPerHub = [];
		$i = 0;
                $weight = 0;
		foreach ($goodsPerSalesOffice as $item) {
			$i++;
                        if($item->sales_office === 'AEHD'){
                            $hub = 'Dubai';
                        }else if ($item->sales_office === 'ITHD'){
                            $hub = 'Brindisi';
                        }else if ($item->sales_office === 'MYHD'){
                            $hub = 'Kuala Lumpur';
                        }else if ($item->sales_office === 'PAHD'){
                            $hub = 'Panama';
                        }else if ($item->sales_office === 'GHHD'){
                            $hub = 'Accra';
                        }else if ($item->sales_office === 'ESHD'){
                            $hub = 'Las Palmas';
                        }
                        if($sum===0){
                            $sum=1;
                        }
                        $percentage = round($item->weight/$sum,3)*100;
                        
                        if($item->sales_office != 'DJHD'){
                            
			$nfiShippedPerHub[] = new PieChart(
                                $hub,$percentage,'#D62331',"pie".$i,'%'
                        );
                        
                        }
                }
		return $nfiShippedPerHub;
	}

	private function nfiShippedPerPartner()
	{
		$goodsPerPartner = Cache::remember('page2.nfiShippedPerPartner.top10', config('unhrd.results_cache_duration'), function() {
			return Shipments::perSoldToParty()->orderBy('weight', 'desc')->get();
		});
		$nfiShippedPerPartner = [];
                $sum = 0;
                $weight =0;
                foreach ($goodsPerPartner as $item) {
                    $sum = $sum + $item->weight;
                }
		foreach ($goodsPerPartner as $item) {
                    $weight = round($item->weight/1000,2);
                    if($sum===0){
                            $sum=1;
                        }
			$nfiShippedPerPartner[] = new ProgressBar(
                                round($item->weight/$sum,2)*100,$item->weight,
                                number_format($item->delivery_gi_value),
				round($item->volume),
                                $item->sold_to_party, "", 
                                array('blue' => 'MT', 'green' => "USD", 'yellow' => "m<sup>3</sup>", 'white' => ""));
		}

		return $nfiShippedPerPartner;
	}

	private function nfiTopFiveShipped()
	{
		$goodsPerSector = Cache::remember('page2.topItems.perSector', config('unhrd.results_cache_duration'), function() {
			return Shipments::perSector()->orderBy('weight', 'desc')->take(4)->get();
		});
                $image = "tents.svg";
		$path = "images/icons/";
		$nfiTopFiveShipped = array();
		foreach ($goodsPerSector as $item) {
                    if($item->sector === 'LOGISTICS'){
                        $image = "logistics.svg";
                    }else if ($item->sector == 'SHELTER'){
                        $image =  "shelter.svg";
                    }else if ($item->sector == 'NUTRITION'){
                        $image =  "nutrition.svg";
                    }else if ($item->sector == 'STAFF SUPPLIES'){
                        $image =  "food.svg";
                    } else if ($item->sector == 'WASH'){
                        $image =  "wash.svg";
                    } else if ($item->sector == 'EDUCATION'){
                        $image =  "education.svg";
                    } else if ($item->sector == 'C.C. CAMP MANAGEMENT'){
                        $image =  "cccm.svg";
                    } else if ($item->sector == 'HEALTH'){
                        $image =  "health.svg";
                    }else {
                        $image = "blankets.svg";
                    }
                    
			$nfiTopFiveShipped[] = new IconPanel(
				title_case_with_exceptions($item->sector),
                                "$path$image",
				round($item->delivery_gi_value/1000000, 1)." M USD");
		}
		usort($nfiTopFiveShipped, function($a, $b) {
			return $a->value < $b->value;
		});

		return $nfiTopFiveShipped;
	}

	private function nfiTopFiveSuppliers()
	{
		$topSuppliers = Cache::remember('page2.topSuppliers.each', config('unhrd.results_cache_duration'), function() {
			return POReportLine::valueByVendor()->ponfPomp()->orderBy('value', 'desc')->take(5)->get();
		});

		$total = Cache::remember('page2.topSuppliers.total', config('unhrd.results_cache_duration'), function() {
			return POReportLine::totals()->ponfPomp()->sum('value_us') + 1;
		});
		
		$nfiTopFiveSuppliers = array();
                $sum = 0;
                foreach ($topSuppliers as $item) {
                    $sum = $sum + $item->value;
                }
                if($sum===0){
                            $sum=1;
                        }
//		foreach ($topSuppliers as $item) {
//			$nfiTopFiveSuppliers[] = new ProgressBar(
//				round($item->value/$sum, 2)*100,"",
//                                number_format($item->value),"",
//				$item->vendor_name, "#1C93BA", 
//                                array('green' => 'USD'));
//		}
                $sum = 10792411.81+1612956.58+1407652.36+893558.67+792156.33;
                $p1 = new ProgressBar(
				round(10792411.81/$sum, 2)*100,"",
                                number_format(10792411.81 ),"",
				"O.B. WIIK AS", "#1C93BA", 
                                array('green' => 'USD'));
                $p2 = new ProgressBar(
				round(1612956.58 /$sum, 2)*100,"",
                                number_format(1612956.58 ),"",
				"EDILSIDER SPA", "#1C93BA", 
                                array('green' => 'USD'));
                $p3 = new ProgressBar(
				round(1407652.36 /$sum, 2)*100,"",
                                number_format(1407652.36 ),"",
				"Farmingtons Automotive GmbH", "#1C93BA", 
                                array('green' => 'USD'));
                $p4 = new ProgressBar(
				round(893558.67 /$sum, 2)*100,"",
                                number_format(893558.67 ),"",
				"Losberger Rapid Deployment Systems", "#1C93BA", 
                                array('green' => 'USD'));
                $p5 = new ProgressBar(
				round(792156.33 /$sum, 2)*100,"",
                                number_format(792156.33 ),"",
				"Verseidag Ballistic Protection Oy", "#1C93BA", 
                                array('green' => 'USD'));
                
                $nfiTopFiveSuppliers[0] = ($p1);
                $nfiTopFiveSuppliers[1] = ($p2);
                $nfiTopFiveSuppliers[2] = ($p3);
                $nfiTopFiveSuppliers[3] = ($p4);
                $nfiTopFiveSuppliers[4] = ($p5);
		return $nfiTopFiveSuppliers;
	}

	//MARK: - Page 3
	private function highValueOperations()
	{
		$highValueSO = Cache::remember('page2.highValueOperations', config('unhrd.results_cache_duration'), function() {
//			return LCRLine::soNumber()->soType()->dndVal()->orderBy('actual_cost', 'desc')->take(5)->get();
                    return LCRLine::soNumber();
		});
                $sum = 0;
		foreach ($highValueSO as $item) {
                    $sum = $sum + $item->actual_cost;
                }
                
		$highValueOperations = array();
                foreach ($highValueSO as $item) {
                    if($sum == 0){
                        $sum =1;
                    }
                    if($item->sales_office === 'AEHD'){
                            $hub = 'Dubai';
                        }else if ($item->sales_office === 'ITHD'){
                            $hub = 'Brindisi';
                        }else if ($item->sales_office === 'MYHD'){
                            $hub = 'Kuala Lumpur';
                        }else if ($item->sales_office === 'PAHD'){
                            $hub = 'Panama';
                        }else if ($item->sales_office === 'GHHD'){
                            $hub = 'Accra';
                        }else if ($item->sales_office === 'ESHD'){
                            $hub = 'Las Palmas';
                        }
                        $so_num = $item->sales_order;
                       
                        $sales_order = "SO# ".$so_num;
                    $highValueOperations[] = new ProgressBar(
                            round($item->actual_cost/$sum, 2)*100,
                            number_format($item->actual_cost),
                            $sales_order,$hub,
                            $item->sold_to_party_name,"",
                            array('blue' => 'USD'));
                }
		return $highValueOperations;
	}

	private function highValueOpenPOsGoods()
	{
		$highValuePOs = Cache::remember('page2.highValueOpenPOsGoods', config('unhrd.results_cache_duration'), function() {
			return POReportLine::poNumberAndValue()->ponfPomp()->livValue()->orderBy('value', 'desc')->take(5)->get();
                
		});
		$highValueOpenPOsGoods = array();
                $path = "images/icons/";
		foreach ($highValuePOs as $item) {
//                     if($item->sector === 'LOGISTICS'){
//                        $image = "logistics.svg";
//                    }else if ($item->sector == 'SHELTER'){
//                        $image =  "shelter.svg";
//                    }else if ($item->sector == 'NUTRITION'){
//                        $image =  "nutrition.svg";
//                    }else if ($item->sector == 'STAFF SUPPLIES'){
//                        $image =  "food.svg";
//                    } else if ($item->sector == 'WASH'){
//                        $image =  "wash.svg";
//                    } else if ($item->sector == 'EDUCATION'){
//                        $image =  "education.svg";
//                    } else if ($item->sector == 'C.C. CAMP MANAGEMENT'){
//                        $image =  "cccm.svg";
//                    } else if ($item->sector == 'HEALTH'){
//                        $image =  "health.svg";
//                    } else {
//                        $image = "blank.png";
//                    }
			$highValueOpenPOsGoods[] = array('title' => $item->po_number,
                            'desc'=>$item->mat_group_desc,
                            'createdby'=>$item->recipient_country, 
                            'value' => number_format($item->value));
                              
		}
                return new POTable(config('unhrd.page3.high_value_po_trans'), array("PO Number","PO Description","Receipient Country","PO Value (USD)"), $highValueOpenPOsGoods);

	}


        private function highValueOpenPOsTransport()
         {
                  $highValuePOs = Cache::remember('page2.highValueOpenPOsTransport', config('unhrd.results_cache_duration'), function() {
                   return POReportLine::poNumberAndValue()->pots()->grValue()->orderBy('value', 'desc')->take(4)->get();
                  });
                  $air = array("U003004","U003008","T003003","T003004","T003012");
                  $land = array("U003003","U003007","T005203","T005207","N002015");
                  $sea = array("U003002","U003006");
                  $path = "images/icons/";
                  $image ="air.svg";
                  $highValueOpenPOsTransport = array();
                  foreach ($highValuePOs as $item) {
                      if (in_array($item->mat_group, $air)) {
                          $image="air.svg";
                      } else if (in_array($item->mat_group,$land)) {
                          $image = "land.svg";
                      } else if (in_array($item->mat_group,$sea)) {
                          $image = "sea.svg";
                      }
                      $po = $item->po_number;
                      $country = $item->recipient_country;
                      $highValueOpenPOsTransport[] =  new IconPanel(
                              title_case_with_exceptions($po),
                                "$path$image",
                               number_format($item->value)." USD");
                  }
                  return $highValueOpenPOsTransport;
         }
	private function overviewTable()
	{
		$overviewTable = Operation::all()->take(10);
		return new OverviewTable(config('unhrd.page3.emergencies'), $overviewTable);
	}
        private function imagePath()
        {
            
            $imagePath = array();
            $files = File::files('uploads');
            foreach ($files as $file)
            {
                $imagePath[] = basename($file);
            }
            return $imagePath;
        }
        
        //MARK: - Page 5
	private function getAnnualFigure()
	{
		$totals = Annual::totals()->material()->get()->first();
//                $totals = Shipments::totals()->get();
//		$perDestinationCount = Cache::remember('page2.perDestinationCount', config('unhrd.results_cache_duration'), function() {
//			return Annual::perDestination()->get()->count();
//		});
                $perDestinationCount =0;
		$totalShipments = Cache::remember('page2.totalShipments', config('unhrd.results_cache_duration'), function() {
			return Annual::select('sales_order')->distinct()->get()->count();
		});

		$totalPartners = Cache::remember('page2.totalPartners', config('unhrd.results_cache_duration'), function() {
			return Annual::perSoldToParty()->get();
		});

		$annualFigures = array();

		$annualFigures['annualTotalWeight'] = number_format($totals->weight/1000, 0, ".", ",");
		$annualFigures['annualTotalVolume'] = number_format($totals->volume, 0, ".", ",");
		$annualFigures['annualTotalValue'] = number_format($totals->value, 0, ".", ",");
		$annualFigures['annualCountriesReached'] = $perDestinationCount;
		$annualFigures['annualShipments'] = $totalShipments;
		$annualFigures['annualTotalPartners'] = count($totalPartners);

		$annualFigures['totalLoans'] = SingleValue::find('total_loans')->value;
		$annualFigures['totalLoanedPartners'] = SingleValue::find('total_loaned_partners')->value;

		return $annualFigures;
	}

	private function nfiShippedPerDepot()
	{
		$goodsPerSalesOffice = Cache::remember('page2.nfiShippedPerHub.each', config('unhrd.results_cache_duration'), function() {
                    return Annual::perSalesOffice()->get();
		});
                $sum =0;
                foreach ($goodsPerSalesOffice as $item) {
                    $sum = $sum + $item->weight;
                }
		$nfiShippedPerHub = [];
		$i = 0;
                $weight = 0;
		foreach ($goodsPerSalesOffice as $item) {
			$i++;
                        if($item->sales_office === 'AEHD'){
                            $hub = 'Dubai';
                        }else if ($item->sales_office === 'ITHD'){
                            $hub = 'Brindisi';
                        }else if ($item->sales_office === 'MYHD'){
                            $hub = 'Kuala Lumpur';
                        }else if ($item->sales_office === 'PAHD'){
                            $hub = 'Panama';
                        }else if ($item->sales_office === 'GHHD'){
                            $hub = 'Accra';
                        }else if ($item->sales_office === 'ESHD'){
                            $hub = 'Las Palmas';
                        }
                        if($sum===0){
                            $sum=1;
                        }
                        $percentage = round($item->weight/$sum,3)*100;
                        
                        if($item->sales_office != 'DJHD'){
                            
			$nfiShippedPerHub[] = new PieChart(
                                $hub,$percentage,'#D62331',"pie".$i,'%'
                        );
                        
                        }
                }
		return $nfiShippedPerHub;
	}

	private function nfiShippedPerPartners()
	{
		$goodsPerPartner = Cache::remember('page2.nfiShippedPerPartners.top10', 
                        config('unhrd.results_cache_duration'), function() {
			return Annual::perSoldToParty()->orderBy('weight', 'desc')->get();
		});
		$nfiShippedPerPartner = [];
                $sum = 0;
                $weight =0;
                foreach ($goodsPerPartner as $item) {
                    $sum = $sum + $item->weight;
                }
		foreach ($goodsPerPartner as $item) {
                    $weight = round($item->weight/1000,2);
                    if($sum===0){
                            $sum=1;
                        }
			$nfiShippedPerPartner[] = new ProgressBar(
                                round($item->weight/$sum,2)*100,$weight,
                                number_format($item->value),
				round($item->volume),
                                $item->sold_to_party, "", 
                                array('blue' => 'MT', 'green' => 'USD', 'yellow' => "m<sup>3</sup>", 'white' => ""));
		}
		return $nfiShippedPerPartner;
	}

	private function nfiTopFivesShipped()
	{
		$goodsPerSector = Cache::remember('page2.topItems.perSector', config('unhrd.results_cache_duration'), function() {
			return Annual::perSector()->orderBy('weight', 'desc')->take(4)->get();
		});
                $image = "tents.svg";
		$path = "images/icons/";
		$nfiTopFiveShipped = array();
		foreach ($goodsPerSector as $item) {
                    if($item->sector === 'LOGISTICS'){
                        $image = "logistics.svg";
                    }else if ($item->sector == 'SHELTER'){
                        $image =  "shelter.svg";
                    }else if ($item->sector == 'NUTRITION'){
                        $image =  "nutrition.svg";
                    }else if ($item->sector == 'STAFF SUPPLIES'){
                        $image =  "food.svg";
                    } else if ($item->sector == 'WASH'){
                        $image =  "wash.svg";
                    } else if ($item->sector == 'EDUCATION'){
                        $image =  "education.svg";
                    } else if ($item->sector == 'C.C. CAMP MANAGEMENT'){
                        $image =  "cccm.svg";
                    } else if ($item->sector == 'HEALTH'){
                        $image =  "health.svg";
                    }else {
                        $image = "blankets.svg";
                    }
                    
			$nfiTopFiveShipped[] = new IconPanel(
				title_case_with_exceptions($item->sector),
                                "$path$image",
				round($item->value/1000000, 1)." M USD");
		}
		usort($nfiTopFiveShipped, function($a, $b) {
			return $a->value < $b->value;
		});
//                dd($nfiTopFiveShipped);
		return $nfiTopFiveShipped;
	}

	private function nfiTopFiveSupplier()
	{
		$topSuppliers = Cache::remember('page2.topSuppliers.each', config('unhrd.results_cache_duration'), function() {
			return POReportLine::valueByVendor()->ponfPomp()->orderBy('value', 'desc')->take(5)->get();
		});

		$total = Cache::remember('page2.topSuppliers.total', config('unhrd.results_cache_duration'), function() {
			return POReportLine::totals()->ponfPomp()->sum('value_us') + 1;
		});
		
		$nfiTopFiveSuppliers = array();
                $sum = 0;
                foreach ($topSuppliers as $item) {
                    $sum = $sum + $item->value;
                }
                if($sum===0){
                            $sum=1;
                        }
		foreach ($topSuppliers as $item) {
			$nfiTopFiveSuppliers[] = new ProgressBar(
				round($item->value/$sum, 2)*100,
                                number_format($item->value),"","",
				$item->vendor_name, "#1C93BA", 
                                array('blue' => 'USD'));
		}

		return $nfiTopFiveSuppliers;
	}
}

