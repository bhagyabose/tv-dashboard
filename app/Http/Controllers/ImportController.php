<?php

namespace Dashboard\Http\Controllers;

use Illuminate\Http\UploadedFile as UploadedFile;
use Illuminate\Http\Request;
use Excel;
use Cache;
use Dashboard\Models\PODLine as PODLine;
use Dashboard\Models\LCRLine as LCRLine;

use Dashboard\Jobs\ProcessExcelChunk as ProcessExcelChunk;
ini_set('max_execution_time', 300);
class ImportController extends Controller
{
	public function import() {
		return view('import.import');
	}

	public function importPost(Request $request) {
		try {
			if($request->fileType == 'pod') {
				$classToUse = "Dashboard\Models\PODLine";
				$quantityField = "delivery_quantity";
			} elseif($request->fileType == 'lcr') {
				$classToUse = "Dashboard\Models\LCRLine";
				$quantityField = "item_quantity";
			} elseif($request->fileType == 'por') {
				$classToUse = "Dashboard\Models\POReportLine";
				$quantityField = "quantity";
                        } elseif($request->fileType == 'pod_annual') {
				$classToUse = "Dashboard\Models\PODAnnual";
				$quantityField = "delivery_quantity";
			} else {
				throw new Exception("Invalid file type", 1);
			}

			$this->importFileWithClass($request->file('report'), $classToUse, $quantityField, $request->session());
			$request->session()->flash('status', "Finished importing file ".$request->file('report')->getClientOriginalName());
			Cache::flush();
		} catch (\Exception $e) {
			echo $e->getMessage().'\n';
			$request->session()->flash('error', $e->getMessage());
		}
		return redirect()->action('ImportController@import');
	}
        
	private function importFileWithClass(UploadedFile $file, $classToUse, $quantityField = "", $session = null) {
		// empty the POD table before import
		$classToUse::truncate()->get();

		config(['excel.import.dates.columns' => ['act_gds_mvmnt_date', 'proof_of_delivery_date']]);

		Excel::filter('chunk')->load($file->getPathname(), null, true)->skip(1)->chunk(250, function($results) use ($classToUse, $quantityField, $session) {
			$rows = $results->toArray();

			foreach ($rows as $row) {
				if( (isset($row['sales_orderpurchase_order']) || isset($row['salespurchase_order_item_no'])) &&
                    ($row['sales_orderpurchase_order'] == 0 || $row['salespurchase_order_item_no'] == 0) ) {
                // || $quantityField != "" && $row[$quantityField] == 0
                    continue;
                }
				try {
					if( isset($row["weight_unit"]) ) {
                        if( $row["weight_unit"] == 'G' ) {
                            $row["total_weight"] = $row["total_weight"] / 1000000;
                        } elseif ( $row["weight_unit"] == 'KG' ) {
                            $row["total_weight"] = $row["total_weight"] / 1000;
                        }    
                    }

					$classToUse::updateOrCreate($row);
				} catch (\Exception $e) {
					$session->flash('error', $e->getMessage());
					continue;
				}
			}
		}, false);
	}
}
