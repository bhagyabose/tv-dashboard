<?php

namespace Dashboard\Http\Controllers;

use Dashboard\Http\Requests;
use Dashboard\Http\Controllers\Controller;

use Dashboard\Models\Operation;
use Illuminate\Http\Request;

class OperationController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$operations = Operation::orderBy('id', 'desc')->paginate(10);

		return view('operations.index', compact('operations'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('operations.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$operation = new Operation();

		$operation->name = $request->input("name");
                $operation->start_date = $request->input("start_date");
                $operation->level = $request->input("level");
                $operation->volume = $request->input("volume");
                $operation->value = $request->input("value");
                $operation->weight = $request->input("weight");
                $operation->partners = $request->input("partners");

		$operation->save();

		return redirect()->route('admin.operations.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$operation = Operation::findOrFail($id);

		return view('operations.show', compact('operation'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$operation = Operation::findOrFail($id);

		return view('operations.edit', compact('operation'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$operation = Operation::findOrFail($id);

		$operation->name = $request->input("name");
                $operation->start_date = $request->input("start_date");
                $operation->level = $request->input("level");
                $operation->volume = $request->input("volume");
                $operation->value = $request->input("value");
		$operation->weight = $request->input("weight");
                $operation->partners = $request->input("partners");
               
		$operation->save();

		return redirect()->route('admin.operations.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$operation = Operation::findOrFail($id);
		$operation->delete();

		return redirect()->route('admin.operations.index')->with('message', 'Item deleted successfully.');
	}

}
