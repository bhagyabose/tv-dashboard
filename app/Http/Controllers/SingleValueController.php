<?php

namespace Dashboard\Http\Controllers;

use Dashboard\Http\Requests;
use Dashboard\Http\Controllers\Controller;

use Dashboard\Models\SingleValue as SingleValue;
use Illuminate\Http\Request;

class SingleValueController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$single_values = SingleValue::orderBy('key', 'desc')->paginate(10);

		return view('single_values.index', compact('single_values'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('single_values.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$single_value = new SingleValue();

		$single_value->key = $request->input("key");
        $single_value->value = $request->input("value");

		$single_value->save();

		return redirect()->route('admin.single_values.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$single_value = SingleValue::findOrFail($id);

		return view('single_values.show', compact('single_value'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$single_value = SingleValue::findOrFail($id);

		return view('single_values.edit', compact('single_value'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$single_value = SingleValue::findOrFail($id);

		$single_value->key = $request->input("key");
        $single_value->value = $request->input("value");

		$single_value->save();

		return redirect()->route('admin.single_values.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$single_value = SingleValue::findOrFail($id);
		$single_value->delete();

		return redirect()->route('admin.single_values.index')->with('message', 'Item deleted successfully.');
	}

}
