<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
	return redirect()->to('tv/page1');
});

Route::group(['prefix' => 'tv'], function () {
	Route::get('page1', ['as' => 'page1', 'uses' => 'TVController@page1']);
	Route::get('page2', ['as' => 'page2', 'uses' => 'TVController@page2']);
	Route::get('page3', ['as' => 'page3', 'uses' => 'TVController@page3']);
        Route::get('page4', ['as' => 'page4', 'uses' => 'TVController@page4']);
        Route::get('page5', ['as' => 'page5', 'uses' => 'TVController@page5']);
});

Route::group(['prefix' => 'api'], function () {
	Route::get('map_data', ['as' => 'map_data', 'uses' => 'APIController@mapData']);
});

Route::group(['prefix' => 'admin'], function () {
	Route::get('rss_items/import', ['as' => 'admin.rss_items.import', 'uses' => 'RSSItemController@import']);
        
	Route::resource("rss_items","RSSItemController");
	Route::resource("partners","PartnerController");
	Route::resource("single_values","SingleValueController");
	Route::resource("operations","OperationController");
        Route::resource("snap_shots","PhotoController");
	Route::group(['prefix' => 'import'], function () {
		Route::get('/', ['as' => 'admin.import.index', 'uses' => 'ImportController@import']);
		Route::post('post', ['as' => 'admin.import.importPost', 'uses' => 'ImportController@importPost']);
                
	});
        Route::group(['prefix' => 'snap_shots'], function () {
		Route::get('/', ['as' => 'admin.snap_shots.index', 'uses' => 'PhotoController@snap_shots']);
                Route::post('post', ['as' => 'admin.image.upload', 'uses' => 'PhotoController@upload']);
                
	});
        
});
