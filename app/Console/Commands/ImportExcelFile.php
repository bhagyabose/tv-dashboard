<?php

namespace Dashboard\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Excel;
use Dashboard\Models\PODLine as PODLine;
use Dashboard\Models\LCRLine as LCRLine;
use Dashboard\Models\POReportLine as POReportLine;

class ImportExcelFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'unhrd:import-excel {file} {chunk=250}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = $this->argument('file');
        try {
            /*if($request->fileType == 'pod') {
                $classToUse = "Dashboard\Models\PODLine";
                $quantityField = "delivery_quantity";
            } elseif($request->fileType == 'lcr') {
                $classToUse = "Dashboard\Models\LCRLine";
                $quantityField = "item_quantity";
            } else {
                throw new Exception("Invalid file type", 1);
            }*/
            $classToUse = "Dashboard\Models\POReportLine";
            $quantityField = "delivery_quantity";

            $this->importFileWithClass($file, $classToUse, $quantityField);
            Log::info("Finished importing file ".$file);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

    private function importFileWithClass($file, $classToUse, $quantityField = "") {
        // empty the POD table before import
        $classToUse::truncate()->get();

        config(['excel.import.dates.columns' => ['po_date', 'po_rel_date', 'del_date']]);

        $chunk = $this->argument('chunk');
        Excel::filter('chunk')->formatDates(true)->load($file, null, true)->skip(1)->chunk($chunk, function($results) use ($classToUse, $quantityField, $chunk) {
            $rows = $results->toArray();
            foreach ($rows as $row) {
                
                if( (isset($row['sales_orderpurchase_order']) || isset($row['salespurchase_order_item_no'])) &&
                    ($row['sales_orderpurchase_order'] == 0 || $row['salespurchase_order_item_no'] == 0) ) {
                // || $quantityField != "" && $row[$quantityField] == 0
                    continue;
                }
                try {
                    if( isset($row["weight_unit"]) ) {
                        if( $row["weight_unit"] == 'G' ) {
                            $row["total_weight"] = $row["total_weight"] / 1000000;
                        } elseif ( $row["weight_unit"] == 'KG' ) {
                            $row["total_weight"] = $row["total_weight"] / 1000;
                        }    
                    }
                    
                    if( isset($row[0]) ) {
                        unset($row[0]);
                    }
                    $ob = new $classToUse();
                    $ob->fill($row);
                    $ob->save();
//                    $classToUse::updateOrCreate($row);
                } catch (\Exception $e) {
                    echo $e->getMessage()."\n";
                    Log::error($e->getMessage());
                    continue;
                }
            }
            echo("Chunk ".$chunk."\n");
        }, false);
    }
}
