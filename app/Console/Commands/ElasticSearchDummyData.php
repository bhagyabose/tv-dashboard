<?php

namespace Dashboard\Console\Commands;

use Illuminate\Console\Command;
use Elasticsearch;

class ElasticSearchDummyData extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'unhrd:dummydata {number=5000}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$sales_order_line_item = 0;

		$number = $this->argument('number');

		for ( $i = 0; $i < $number; $i++ )
		{
			if($i % 100 == 0)
			{
				echo $i."/".$number."\n";
			}

			if($sales_order_line_item == 0)
			{
				$sales_order = random_int(1, 1000);
				$sales_order_line_item = random_int(1, 10);
			}

			$sales_order_line_item--;

			$line_item = [
			"sales_order" => $sales_order,
			"so_item" => $sales_order_line_item,
			"sales_office" => $this->randomSalesOffice(),
			"sold_to" => $this->randomPartner(),
			"shipped_to" => $this->randomPartner(),
			"country" => $this->randomCountry(),
			"weight" => random_int(1, 5000),
			"volume" => random_int(1, 5000),
			"value" => random_int(1, 5000),
			"acgi_date" => $this->randomDate('2014-01-01', '2016-09-07', 'Y-m-d'),
			"pod_date" => $this->randomDate('2014-01-01', '2016-09-07', 'Y-m-d'),
			"category" => $this->randomCategory()];

			$data = [
			'index' => 'line_items',
			'type' => 'line_item',
			'id' => $i,
			'body' => $line_item
			];
			$return = Elasticsearch::index($data);
		}
	}

	private function randomFromArray($array)
	{
		return $array[random_int(0, count($array)-1)];
	}

	private function randomCountry()
	{
		$countries = ["CF" => random_int(1, 10), "CD" => random_int(1, 10), "IQ" => random_int(1, 10), "SO" => random_int(1, 10), "SS" => random_int(1, 10), "SY" => random_int(1, 10), "UA" => random_int(1, 10), "YE" => random_int(1, 10), "KG" => random_int(1, 10), "LA" => random_int(1, 10), "HT" => random_int(1, 10)];

		return $this->random_probability($countries);
	}

	private function randomSalesOffice()
	{
		$salesOffices = ["ITHD" => random_int(1, 10), "MYHD" => random_int(1, 10), "AEHD" => random_int(1, 10), "GHHD" => random_int(1, 10), "PAHD" => random_int(1, 10), "ESHD" => random_int(1, 10), "DJHD" => random_int(1, 10)];

		return $this->random_probability($salesOffices);
	}

	private function randomPartner()
	{
		$partners = ["ACTION CONTRE LA FAIM", "Agence  d Aide  a la  Cooperation", "AGENCIA ESPANOLA DE COOPERACION INT", "ASEAN Coordinating Centre for Human", "CARE USA", "Catholic Relief Services", "CATHOLIC RELIEF SERVICES-USCCB", "CO - Afghanistan", "CO - Bolivia", "CO - Burkina Faso", "CO - Burundi", "CO - Cambodia", "CO - Cameroon", "CO - Central African Republic", "CO - Chad", "CO - Congo D.R.", "CO - Cuba", "CO - DJIBUTI", "CO - DPR Korea", "CO - El Salvador", "CO - Ethiopia", "CO - Gambia", "CO - Ghana", "CO - Guatemala", "CO - Guinea", "CO - Honduras", "CO - Indonesia", "CO - Iraq", "CO - Kenya", "CO - KYRGHIZSTAN", "CO - Lesotho", "CO - Liberia", "CO - Madagascar", "CO - Malawi", "CO - Mali", "CO - Mauritania", "CO - Mozambique", "CO - Nepal", "CO - Nicaragua", "CO - Niger", "CO - Pakistan", "CO - Palestinian Territory", "CO - PHILIPPINES", "CO - Rwanda", "CO - Senegal", "CO - Sierra Leone", "CO - Somalia", "CO - Sudan", "CO - Syria", "CO - Tanzania", "CO - TURKEY", "CO - Uganda", "CO - Yemen", "CO SOUTH SUDAN", "CO-MYANMAR", "COMISION CASCOS BLANCOS ARGENTINA", "COMITATO ITALIANO PER IL PROGRAMMA", "CONCERN WORLDWIDE", "Cooperazione Italiana allo Sviluppo", "Department of Foreign Affairs and T", "Deutsche Welthungerhilfe e.V.", "FAO", "FRANCE DIPLOMATIE MINISTERE DES AFF", "Fundacion Accion Contra El Hambre", "FUNDACION MUJERES POR AFRICA", "GVLP - GLOBAL VEHICLE LEASING PROJE", "Handicap International Federation", "INTERMON OXFAM", "International  Medical  Corps", "INTERNATIONAL HUMANITARIAN CITY", "INTERNATIONAL ORGANIZATION FOR MIGR", "INTERSOS", "Irish Aid", "ISLAMIC RELIEF HQ", "Japan International Cooperation Age", "KOREA INTERNATIONAL COOPERATION AGE", "Lutheran World Relief", "MERCY CORPS", "MERCY MALAYSIA", "Norwegian Church Aid", "NORWEGIAN REFUGEE COUNCIL", "OFICINA TECNICA DE COOPERACION EN P", "PAN AMERICAN HEALTH ORGANIZATION /", "QATAR CHARITY", "RB - ASIA BUREAU (BANGKOK)", "RB - LATIN AMERICA & CARIBBEAN", "RB - M.East, C.Asia & E.Europ", "SAVE THE CHILDREN INTERNATIONAL", "SHELTER BOX", "SOLIDARITES INTERNATIONAL", "SOLIDARITY", "Swiss Agency for Development and Co", "THE SWISS RED CROSS", "UN High Commissioner for Refugees U", "UNDP NY HQ", "UNDP SOMALIA", "UNFPA", "UNHRD - Dubai", "UNHRD BRINDISI", "UNICEF SUPPLY DIVISION", "United Nations High Commissioner fo", "United Nations Office for the Coord", "United Nations Population Fund", "UNITED NATIONS RELIEF AND WORKS AGE", "WFP", "WFP DUBAI / FITTEST", "WFP HQ", "WFP Support Office, UAE", "WORLD HEALTH ORGANIZATION", "WORLD HEALTH ORGANIZATION (WHO)", "World Vision International"];

		return $this->randomFromArray($partners);
	}

	private function randomCategory()
	{
		$categories = ["Pharmaceuticals", "Mixed Food", "Supplementary Feeding", "Field Supplies", "Therapeutic Feeding", "Housing Equipment", "Safety Gears", "Plastic Sheeting", "Tents", "Office Equipment & Supplies", "Other Camp Management Equipment", "Warehousing", "Other Protection Supplies", "Hygiene", "Health Kits", "Water", "Hospital Equipment", "Sanitation", "School", "Immunisation", "Laboratory Supplies", "Recreation", "Transport", "Agriculture Tools", "Satellite Communication Equipment", "VHF Equipment", "HF Equipment", "Other Early Recovery Equipment"];

		return $this->randomFromArray($categories);
	}

	private function randomDate($sStartDate, $sEndDate, $sFormat = 'Y-m-d H:i:s')
	{
		// Convert the supplied date to timestamp
		$fMin = strtotime($sStartDate);
		$fMax = strtotime($sEndDate);
		// Generate a random number from the start and end dates
		$fVal = mt_rand($fMin, $fMax);
		// Convert back to the specified date format
		return date($sFormat, $fVal);
	}

	function random_probability($probabilities) {
		$rand = rand(0, array_sum($probabilities));
		do {
			$sum = array_sum($probabilities);
			if($rand <= $sum && $rand >= $sum - end($probabilities)) {
				return key($probabilities);
			}
		} while(array_pop($probabilities));
	}
}
