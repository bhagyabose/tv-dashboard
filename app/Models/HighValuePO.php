<?php

namespace Dashboard\Models;

use DB;

use Illuminate\Database\Eloquent\Model;

class HighValuePO extends Model
{
    public $table = 'highvaluepo';
    
    public function scopePonfPomp($query)
    {
        return $query->whereIn('type', array('PONF','POMP'));
    }

    public function scopePots($query)
    {
            return $query->whereIn('type', array('POTS'));
    }

	public function scopeTotals($query)
    {
        return $query->select(DB::raw('sum(value_us) as value'));
    }

    public function scopeLivValue($query)
    {
    	return $query->where('liv_value', "=", "0.00")->orWhereNull('liv_value'); 
    }
	
	public function scopeGrValue($query)
    {
    	return $query->where('gr_value', "=", "0.00")->orWhereNull('gr_value'); 
    }

    public function scopePoNumberAndValue($query)
    {
    	return $query->select('po_number','mat_group','mat_group_desc','recipient_country', DB::raw('sum(value_us) as value'))->groupBy('po_number');
    }
    
}
