<?php

namespace Dashboard\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence; // base trait
use Sofa\Eloquence\Mappable; // extension trait

class ImageImporter extends Model
{
	use HasCompositePrimaryKey, Eloquence, Mappable;

	protected $table = 'snap_shots';

//	protected $primaryKey = array('image_id');

	protected $fillable = ["image"];

	public $timestamps = false;

}
