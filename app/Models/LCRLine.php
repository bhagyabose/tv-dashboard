<?php

namespace Dashboard\Models;

use DB;

use Illuminate\Database\Eloquent\Model;

class LCRLine extends Model
{
	use HasCompositePrimaryKey;

	protected $table = 'logistics_reconciliation_report';

	protected $primaryKey = array('sales_order', 'so_item');

	protected $fillable = ["so_type" , "so_date" , "sales_order" , 
            "so_item" , "material" , "item_quantity" , "so_item_description" , 
            "sales_office" , "sold_to_party" , "sold_to_party_name" , 
            "ship_to_party" , "ship_to_party_name" , "actual_cost_usd" , 
            "mrc_value_usd" , "order_reason" , "rejectionreason" , "created_on" , 
            "created_by" , "wbs_number" , "wbs_description" , "pi_number" , 
            "pi_value" , "pi_date" , "pi_status" , "debit_note_draft" , 
            "dnd_value" , "dnd_date" , "dnd_status" , "debit_note_reld" , 
            "dnr_value" , "dnr_date" , "dnr_status" , "int_po_number" , 
            "int_po_item" , "vendor" , "int_po_value" , "int_po_rel_status" , 
            "int_po_rel_date" , "delivery_number" , "del_tot_weight" , 
            "del_tot_volume" , "volume_unit" , "del_tot_value" , "delivery_status" , 
            "proof_of_del_status" , "pr_number" , "pr_item" , "pr_release_status" , 
            "pr_release_date" , "ext_po_number" , "ext_po_item" , "ext_po_rel_status" , 
            "ext_po_rel_date" , "po_currency" , "report_currency" , "po_item_val" , 
            "ses_number" , "ses_item" , "ses_item_qty" , "ses_item_value" , "grn" , 
            "grn_item" , "grn_qty" , "grn_value" , "liv" , "liv_item" , "liv_qty" , 
            "liv_value" , "subsqt_drcr" , "subsqt_item" , "sbsqt_item_qty" , 
            "sbsqt_item_value" , "incoterms_2"];

	public $timestamps = false;
        
        public function scopeSoNumber($query)
        {
//            return $query->select('sales_order','sales_office','sold_to_party_name',
//                    DB::raw('actual_cost_usd as actual_cost'))
//                    ->groupBy('sales_order')->orderBy('actual_cost', 'desc');
            $query = DB::select('SELECT `sales_order`, sum(`actual_cost_usd`) as actual_cost, `sales_office`, `sold_to_party_name` FROM ( SELECT DISTINCT `sales_order`, `so_item`, `actual_cost_usd`, `sales_office`, `sold_to_party_name` FROM `logistics_reconciliation_report` WHERE `so_type` in ("ZHNO","ZHWO") AND dnd_value=0 ) summary GROUP BY `sales_order` ORDER BY sum(`actual_cost_usd`) DESC LIMIT 5');
            return $query;
        }
        
        public function scopeSoType($query)
        {
            return $query->whereIn('so_type',array('ZHNO','ZHWO'));
        }
        
        public function scopeDndVal($query)
        {
            return $query->where('dnd_value','=',0.00)->orWhereNull('dnd_value');
        }
        
        public function scopeMaterial ($query){
            return $query->where('material', 'like', 'B%');
        }

    
}
