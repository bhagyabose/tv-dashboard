<?php

namespace Dashboard\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon as Carbon;

class ShipmentsAnnual extends BaseModel
{
//    public $table = 'shipments_annual';
    protected $readFrom = 'shipments_annual';

    /**
     * Actual good use of a scope: to limit the results to last year.
     */
    public function scopeLastYear($query)
    {
        return $query->whereBetween('act_gds_mvmnt_date', [Carbon::now()->subYear(), Carbon::now()]);
    }

    /**
     * Scope to return totals per sales_office
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePerSalesOffice($query)
    {
        return $query->select('sales_office', DB::raw('sum(weight) as weight'))->where('sales_office', '!=', 'DJHD')->groupBy('sales_office');

    }

    /**
     * Scope to return totals per destination
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePerDestination($query)
    {
        return $query->select('country', DB::raw('sum(weight) as weight'),DB::raw('sum(volume) as volume'),DB::raw('sum(delivery_gi_value) as delivery_gi_value'))->groupBy('country');
    }


    /**
     * Scope to return totals per sector
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePerSector($query)
    {
        return $query->select('sector', DB::raw('sum(weight) as weight'),DB::raw('sum(volume) as volume'),DB::raw('sum(delivery_gi_value) as delivery_gi_value'))->where('sector', 'not like', 'NULL')->groupBy('sector');
    }

    /**
     * Scope to return totals per sold to party
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePerSoldToParty($query)
    {
//        $wfp = DB::table($this->readFrom)->select(DB::raw('"WORLD FOOD PROGRAMME" as sold_to_party'), DB::raw('"images/partners/wfp.svg" as logo'), DB::raw('sum(weight) as weight'),DB::raw('sum(volume) as volume'), DB::raw('sum(delivery_gi_value) as delivery_gi_value'))->where('sold_to_party_code', 'like', '6%')->whereBetween('act_gds_mvmnt_date', [Carbon::now()->subYear(), Carbon::now()]);
//        $rest = DB::table($this->readFrom)->select('sold_to_party', DB::raw('sold_to_party_logo as logo'), DB::raw('sum(weight) as weight'),DB::raw('sum(volume) as volume'), DB::raw('sum(delivery_gi_value) as delivery_gi_value'))->where('sold_to_party_code', 'not like', '6%')->groupBy('sold_to_party')->whereBetween('act_gds_mvmnt_date', [Carbon::now()->subYear(), Carbon::now()]);
        $wfp = DB::table($this->readFrom)->select(DB::raw('"WORLD FOOD PROGRAMME" as sold_to_party'), DB::raw('"images/partners/wfp.svg" as logo'), DB::raw('sum(weight) as weight'),DB::raw('sum(volume) as volume'), DB::raw('sum(delivery_gi_value) as delivery_gi_value'))->where('sold_to_party_code', 'like', '6%');
        $rest = DB::table($this->readFrom)->select('sold_to_party', DB::raw('sold_to_party_logo as logo'), DB::raw('sum(weight) as weight'),DB::raw('sum(volume) as volume'), DB::raw('sum(delivery_gi_value) as delivery_gi_value'))->where('sold_to_party_code', 'not like', '6%')->groupBy('sold_to_party');

//        $rest = DB::table($this->readFrom)->select('sold_to_party',DB::raw('sum(weight) as weight'),DB::raw('sum(volume) as volume'), DB::raw('sum(delivery_gi_value) as delivery_gi_value'))->groupBy('sold_to_party');
        return $wfp->union($rest);
//        return $rest;
    }

    /**
     * Scope to return totals
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTotals($query)
    {
        return $query->select(DB::raw('sum(weight) as weight'), DB::raw('sum(volume) as volume'),DB::raw('sum(delivery_gi_value) as value'));
    }

    /**
     * Scope to return current shipments countries
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeShipmentCountryPairs($query)
    {
        //return $query->select('sales_office', 'country_key', "latitude", "longitude")->distinct();
        // Removing Djibouti
        return $query->select('sales_office', 'country_key', "latitude", "longitude")->where('sales_office', '!=', 'DJHD')->distinct();
    }
}
