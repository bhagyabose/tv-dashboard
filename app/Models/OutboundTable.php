<?php

namespace Dashboard\Models;

use Illuminate\Database\Eloquent\Model;

class OutboundTable extends Model
{
    protected $table = 'so_outbound_table';

    protected $primaryKey = 'Sales_order';

    public function lineItems()
    {
    	return $this->hasMany('Dashboard\Models\LineItem', 'Sales_order', 'Sales_order');
    }

    public function dispatches()
    {
	return $this->hasMany('Dashboard\Models\Dispatch', 'Sales_order', 'Sales_order');	
    }


}
