<?php

namespace Dashboard\Models\Charts;

use Illuminate\Database\Eloquent\Model;

class PieChart extends Model
{
	public $divId;
	public $label;
	public $value; 
	public $fillColor;
	public $unit;

	public function __construct ( $label = "Chart without a label", $value = 0, $fillColor = '#2A93FC', $divId = '', $unit = '' ) {
		$this->divId = $divId;
		$this->label = $label;
		$this->value = $value; 
		$this->fillColor = $fillColor;
		$this->unit = $unit;
	}
}
