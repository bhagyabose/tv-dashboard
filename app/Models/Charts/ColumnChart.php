<?php

namespace Dashboard\Models\Charts;

//use Illuminate\Database\Eloquent\Model;
//extends Model
class ColumnChart
{
	public $divId;
	public $title;
	public $items;
	public $fillColors;
	public $unit;

	public function __construct ( $title = "Chart without a title", $items = [], $fillColors = '#2A93FC', $divId = '', $unit = 'mt' ) {
		$this->divId = $divId;
		$this->title = $title;
		$this->items = $items;
		$this->fillColors = $fillColors;
		$this->unit = $unit;
	}
}
