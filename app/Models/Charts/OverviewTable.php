<?php

namespace Dashboard\Models\Charts;

use Illuminate\Database\Eloquent\Model;

class OverviewTable extends Model
{
	public $title;
	public $rows;

	public function __construct ( $title, $rows) {
		$this->title = $title;
		$this->rows = $rows;
	}
}
