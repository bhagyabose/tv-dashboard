<?php

namespace Dashboard\Models\Charts;

//use Illuminate\Database\Eloquent\Model;
//extends Model
class IconPanel
{
	public $title;
	public $icon;
	public $value;

	public function __construct ( $title = "Panel without a text", $icon = "", $value = "" ) {
		$this->title = $title;
		$this->icon = $icon;
		$this->value = $value;
	}
}
