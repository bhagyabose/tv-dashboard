<?php

namespace Dashboard\Models\Charts;

use Illuminate\Database\Eloquent\Model;

class POList extends Model
{
	public $title;
	public $values;

	public function __construct ( $title, $values) {
		$this->title = $title;
		$this->values = $values;
	}
}
