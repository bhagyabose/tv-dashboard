<?php

namespace Dashboard\Models\Charts;

use Illuminate\Database\Eloquent\Model;

class POTable extends Model
{
	public $title;
	public $header;
	public $values;

	public function __construct ( $title, $header, $values) {
		$this->title = $title;
		$this->header = $header;
		$this->values = $values;
	}
}
