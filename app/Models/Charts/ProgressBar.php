<?php

namespace Dashboard\Models\Charts;


class ProgressBar
{
  public $percentage;
  public $blue;
  public $green;
  public $yellow;
  public $white;
  public $color;
  public $units;

//  public function __construct ( $percentage, $blue = "", $green = "", $yellow = "", $white = "", $color = "#2A93FC", $units = []) {
  public function __construct ( $percentage, $blue = "", $green = "", $yellow = "", $white = "", $color = "", $units = []) {
    $this->percentage = $percentage;
    $this->blue = $blue;
    $this->green = $green;
    $this->yellow = $yellow;
    $this->white = $white;
    $this->color = $color;
    $this->units = $units;
  }

	/*public $percentage;
  public $value;
  public $volume;
  public $organisation;
  public $weight;	
  public $color;

  public function __construct ( $organisation, $percentage, $value = "", $volume = "", $weight = "", $color = "#2A93FC") {
    $this->organisation = $organisation;
    $this->percentage = $percentage;
    $this->value = $value;
    $this->volume = $volume;
    $this->weight = $weight;
    $this->color = $color;
  }*/
}
