<?php

namespace Dashboard\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence; // base trait
use Sofa\Eloquence\Mappable; // extension trait

class PODAnnual extends Model
{
    use HasCompositePrimaryKey, Eloquence, Mappable;

	protected $table = 'pod_annual';

	protected $primaryKey = array('sales_orderpurchase_order', 'salespurchase_order_item_no');

	/*	protected $fillable = ["delivery", "soponum", "pi_number", "sold_to_pt", "name_of_sold_to_party", "name_of_the_ship_to_party", "cty", "location_of_the_ship_to_party", "material", "description", "plnt", "soff", "incot", "incoterms_part_2", "geographical_area", "dlvty", "itca", "gm", "gs", "ops", "wm", "wmstat", "st", "sopoitem", "acgi_date", "pod_date", "delivery_quantity", "su", "total_weight", "wun", "net_weight", "volume", "vun", "delivery_gi_value", "curr", "wbs_element"];*/

	protected $fillable = ["delivery", "sales_orderpurchase_order", "salespurchase_order_item_no", "pi_number", "act_gds_mvmnt_date", "proof_of_delivery_date", "sold_to_party", "name_of_sold_to_party", "name_of_the_ship_to_party", "country_key", "location_of_the_ship_to_party", "material", "description", "delivery_quantity", "sales_unit", "total_weight", "weight_unit", "net_weight", "volume", "volume_unit", "delivery_gi_value", "statistics_currency", "wbs_element", "description2", "description3", "plant", "sales_office", "incoterms", "incoterms_part_2", "geographical_area", "delivery_type", "item_category", "total_gds_mvt_stat", "goods_movement_stat", "overall_pickstatus", "overall_wm_status", "wm_activity_status", "shipping_type"];

	public function scopePartner($query)
    {
        return $query->select('sold_to_party', 'name_of_sold_to_party')->distinct()->get();
    }

	public $timestamps = false;

	protected $dates = ['act_gds_mvmnt_date', 'proof_of_delivery_date'];
}
