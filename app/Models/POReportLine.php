<?php

namespace Dashboard\Models;

use DB;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence; // base trait
use Sofa\Eloquence\Mappable; // extension trait

class POReportLine extends Model
{
	use HasCompositePrimaryKey, Eloquence, Mappable;

	protected $table = 'po_report';

	protected $primaryKey = array('po_number', 'item');

	/*	protected $fillable = ["delivery", "soponum", "pi_number", "sold_to_pt", "name_of_sold_to_party", "name_of_the_ship_to_party", "cty", "location_of_the_ship_to_party", "material", "description", "plnt", "soff", "incot", "incoterms_part_2", "geographical_area", "dlvty", "itca", "gm", "gs", "ops", "wm", "wmstat", "st", "sopoitem", "acgi_date", "pod_date", "delivery_quantity", "su", "total_weight", "wun", "net_weight", "volume", "vun", "delivery_gi_value", "curr", "wbs_element"];*/

	protected $fillable = ['po_number', 'item', 'po_date', 'po_rel_date', 'po_releasor', 'porg', 'pgr', 'creator', 'type', 'vendor', 'vendor_name', 'vendor_country_code', 'vendor_country', 'recipient_country', 'mat_group', 'mat_group_desc', 'mat_grp_detail', 'description', 'quantity', 'uom', 'unit_price', 'po_val_loc', 'curr', 'value_us', 'del_date', 'dc', 'd', 'inco', 'pr_no', 'itm', 'coll_no', 'lta_no', 'gr_value', 'liv_value', 'po_gr_value', 'gr_liv_value', 'ba', 'fa', 'grant', 'fund', 'ccntr', 'order', 'wbs_element', 'asset'];

	public $timestamps = false;

	protected $dates = ['po_date', 'po_rel_date', 'del_date'];

	public function scopePonfPomp($query)
	{
            return $query->whereIn('type', array('PONF','POMP', 'PONW'));
	}

	public function scopePots($query)
	{
		return $query->whereIn('type', array('POTS'));
	}

	public function scopeTotals($query)
    {
        return $query->select(DB::raw('sum(value_us) as value'));
    }

    public function scopeLivValue($query)
    {
    	return $query->where('liv_value', "=", "0.00")->orWhereNull('liv_value'); 
    }
	
	public function scopeGrValue($query)
    {
    	return $query->where('gr_value', "=", "0.00")->orWhereNull('gr_value'); 
    }

    public function scopePoNumberAndValue($query)
    {
    	return $query->select('po_number','mat_group_desc','mat_group','mat_group_desc','recipient_country', DB::raw('sum(value_us) as value'))->groupBy('po_number');
    }

    public function scopeValueByVendor($query)
    {
        return $query->select('vendor_name', DB::raw('sum(value_us) as value'))->groupBy('vendor_name');
    }
}
