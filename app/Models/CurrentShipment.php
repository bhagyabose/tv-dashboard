<?php

namespace Dashboard\Models;

use Illuminate\Database\Eloquent\Model;

class CurrentShipmentLine extends BaseModel
{
    protected $readFrom = 'current_shipments';
}
 