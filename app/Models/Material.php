<?php

namespace Dashboard\Models;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
	protected $table = 'material';
    protected $fillable = ['idMATERIAL', 'CLUSTER_DESCRIPTION', 'DESCRIPTION', 'SECTOR', 'CATEGORY', 'FAMILY'];
	public $timestamps = false;
}
