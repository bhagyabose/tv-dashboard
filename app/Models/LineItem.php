<?php

namespace Dashboard\Models;

use Illuminate\Database\Eloquent\Model;

class LineItem extends Model
{
    protected $table = 'so_line_items';

    public function outboundTable()
    {
	return $this->belongsTo('Dashboard\Models\OutboundTable', 'Sales_order', 'Sales_order');
    } 
}
