<?php

namespace Dashboard\Models;

use Illuminate\Database\Eloquent\Model;

class RSSItem extends Model
{
    protected $table = "rss_items";

    protected $visible = ['text'];
}
