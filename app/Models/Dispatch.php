<?php

namespace Dashboard\Models;

use Illuminate\Database\Eloquent\Model;

class Dispatch extends Model
{
    protected $table = 'dispatch';

    public function outboundTable()
    {
	return $this->belongsTo('Dashboard\Models\OutboundTable', 'Sales_order', 'Sales_order');
    }

    public function scopeCurrent($query)
    {
        return $query->where('warehouse_load_date', '!=', '0000-00-00')->where('pod_date_received', '=', '0000-00-00');
    }
}
 