<?php

namespace Dashboard\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class LogisticsAnnual extends Model
{
    protected $table = 'logistics_annual';
    
    public function scopePerSalesOffice($query)
    {
        return $query->select('sales_office', DB::raw('sum(weight) as weight'))
                ->where('sales_office', '!=', 'DJHD')->groupBy('sales_office');

    }
    public function scopePerSector($query)
    {
        return $query->select('sector', DB::raw('sum(weight) as weight'),
                DB::raw('sum(volume) as volume'),
                DB::raw('sum(actual_cost_usd) as value'))
                ->where('sector', 'not like', 'NULL')->groupBy('sector');
    }

    /**
     * Scope to return totals per sold to party
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePerSoldToParty($query)
    {
        $wfp = DB::table($this->table)->select(DB::raw('"WORLD FOOD PROGRAMME" as sold_to_party'), 
                DB::raw('"images/partners/wfp.svg" as logo'), 
                DB::raw('sum(weight) as weight'),
                DB::raw('sum(volume) as volume'), 
                DB::raw('sum(actual_cost_usd) as value'))
                ->where('sold_to_party_code', 'like', '6%');
        $rest = DB::table($this->table)->select('sold_to_party', 
                DB::raw('sold_to_party_logo as logo'), 
                DB::raw('sum(weight) as weight'),
                DB::raw('sum(volume) as volume'), 
                DB::raw('sum(actual_cost_usd) as value'))
                ->where('sold_to_party_code', 'not like', '6%')
                ->groupBy('sold_to_party');
        return $wfp->union($rest);
//        return $query->select('sold_to_party',
//                DB::raw('sum(weight) as weight'),
//                DB::raw('sum(volume) as volume'), 
//                DB::raw('sum(actual_cost_usd) as value'))->groupBy('sold_to_party');;
    }

    /**
     * Scope to return totals
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTotals($query)
    {
        return $query->select(DB::raw('sum(weight) as weight'), 
                DB::raw('sum(volume) as volume'),
                DB::raw('sum(actual_cost_usd) as value'));
    }
    
    /**
     * Material code filter
     */
    public function scopeMaterial($query)
    {
        return $query->whereIn('material', array('UNHRD INT DD','UNHRD EXT DD'))
                ->orWhere('material', 'like', 'B%');
        
    }
    
}
