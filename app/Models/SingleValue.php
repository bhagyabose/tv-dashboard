<?php

namespace Dashboard\Models;

use Illuminate\Database\Eloquent\Model;

class SingleValue extends Model
{
    protected $primaryKey = 'key';
    public $incrementing = false;
    public $timestamps = false;
}
