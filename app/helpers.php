<?php

function salesOfficeData($code, $variable) {
	$depots = config('unhrd.depots');
	return array_key_exists($code, $depots) ? $depots[$code][$variable] : $code;
}

function salesOfficeForCode($code) {
	return salesOfficeData($code, "name");
}

function salesOfficeCountryCode($code) {
	return salesOfficeData($code, "country_code");
}

function salesOfficeCoordinates($code) {
	return salesOfficeData($code, "coordinates");
}

function title_case_with_exceptions($string) {
	if($string == "WASH") {
		return strtoupper($string);
	}
	return title_case($string);
}